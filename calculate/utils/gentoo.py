import os
import re
from typing import Dict
from calculate.utils.files import read_file_lines


class ProfileWalker:
    '''Объект обходящий все директории профиля через parent файлы.'''
    def __init__(self, filename: str, repositories: Dict[str, str]):
        self.repositories: Dict[str, str] = repositories
        self.filename: str = filename
        self.re_reppath: re.Pattern = re.compile("^({0})+:".format(
                                           "|".join(self.repositories.keys())))

    def interpolate(self, path: str):
        def subfunc(m):
            return "{0}/profiles/".format(self.repositories.get(m.group(1)))
        return self.re_reppath.sub(subfunc, path)

    def find(self, directory: str):
        '''Метод для поиска по профилю всех файлов с именем, указанным в
        self.filename.'''
        parent_file_path = os.path.join(directory, "parent")
        for line in (parent_line for parent_line in
                     read_file_lines(parent_file_path) if parent_line.strip()):
            parent_directory = os.path.normpath(
                               os.path.join(directory, self.interpolate(line)))
            if os.path.exists(parent_directory):
                yield from self.find(parent_directory)
        findfile = os.path.normpath(os.path.join(directory, self.filename))
        if os.path.exists(findfile):
            yield findfile
