import os
from os.path import exists


def readlink(path):
    try:
        return os.readlink(path)
    except FileNotFoundError:
        return None


def readFileLines(path):
    if exists(path):
        with open(path, 'r') as f:
            for line in f:
                yield line.rstrip("\n")


def readFile(path):
    if exists(path):
        with open(path, 'r') as f:
            return f.read()
