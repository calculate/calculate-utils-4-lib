from calculate.variables.datavars import (
    Variable,
    Namespace,
    Dependence,
    StringType,
    BooleanType,
    HashType,
    ListType,
    Calculate,
    Copy
    )
from calculate.vars.main.os.func import get_arch_gentoo
from calculate.vars.install.os.func import (get_audio_selected,
                                            get_available_audio_system)


def import_variables():
    with Namespace("arch"):
        Variable("machine", type=StringType,
                 source=Copy("main.os.arch.machine"))

        Variable("gentoo", type=StringType,
                 source=Calculate(get_arch_gentoo, ".machine"))

    with Namespace("audio"):
        Variable("available", type=ListType,
                 source=Calculate(
                            get_available_audio_system))

        Variable("selected", type=StringType,
                 source=Calculate(
                            get_audio_selected,
                            ".available",
                            "main.cl.cmdline.calculate.audio"))

    Variable("subsystem", type=StringType, source=Copy("main.os.subsystem"))

    with Namespace("container"):
        Variable("type", type=StringType,
                 source=Copy("main.os.container.type"))

    with Namespace("linux"):
        Variable("shortname", type=StringType,
                 source=Copy("main.os.linux.shortname"))

        Variable("name", type=StringType,
                 source=Copy("main.os.linux.name"))

        Variable("subname", type=StringType,
                 source=Copy("main.os.linux.subname"))

        Variable("system", type=StringType,
                 source=Copy("main.os.linux.system"))

        Variable("ver", type=StringType,
                 source=Copy("main.os.linux.ver"))

        Variable("build", type=StringType,
                 source=Copy("main.os.linux.build"))
