from calculate.utils.package import PackageAtomParser


def get_available_audio_system():
    audio_systems = (
        ('alsa', None),
        ('pulseaudio', 'media-sound/pulseaudio')
    )
    package_db = PackageAtomParser()
    return [
        audio_system
        for audio_system, pkg in audio_systems
        if pkg is None or package_db.is_package_exists(pkg)
    ]


def get_audio_selected(available_systems, cmdline_audio):
    available_systems = available_systems.value

    if 'pulseaudio' in available_systems:
        cmdline_audio = cmdline_audio.value
        if cmdline_audio and cmdline_audio == 'alsa':
            return 'alsa'
        else:
            return 'pulseaudio'
    return 'alsa'
