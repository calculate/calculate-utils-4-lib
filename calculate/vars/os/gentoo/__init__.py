import os
from calculate.utils.files import stderr_devnull
from calculate.utils.files import read_link, FilesError
from calculate.variables.datavars import Variable, Namespace, Dependence,\
                                         StringType, TableType


def import_variables():
    '''
    gentoo:
          make_profile -> string
          profile:
              path -> string
              name -> string
          repositories[*]{name, path} -> table
          config -> undefined
    '''

    # Путь до файла, указывающего на активный профиль
    Variable('make_profile', type=StringType,
             source='/etc/portage/make.profile')

    # Параметры текущего профиля.
    with Namespace('profile'):
        def get_profile_link(make_profile):
            make_profile_dir = os.path.dirname(make_profile.value)
            try:
                profile_link = read_link(make_profile.value)
            except FilesError:
                return ""

            if profile_link:
                profile_link = os.path.normpath(os.path.join(make_profile_dir,
                                                             profile_link))
                return profile_link
            else:
                return ""
        # Абсолютный путь до профиля
        Variable('path', type=StringType,
                 source=Dependence('..make_profile', depend=get_profile_link))

        def get_profile_name(path, repositories):
            profile_path = path.value
            if not profile_path:
                return ""

            for repository in repositories.value:
                repository_path = repository['path']
                repository_name = repository['name']
                remove_part = os.path.normpath(os.path.join(repository_path,
                                                            "profiles"))
                if profile_path.startswith(remove_part):
                    return "{}:{}".format(repository_name,
                                          profile_path[len(remove_part) + 1:])
            return profile_path
        # Название профиля
        Variable('name', type=StringType,
                 source=Dependence('.path', '..repositories',
                                   depend=get_profile_name))

    def get_repository_table(config):
        return [{'name': name,
                 'path': path}
                for path, name
                in config.value.repositories.location_map.items()]

    # Информация о репозиториях
    # name: имя репозитория
    # path: полный путь до репозитория
    Variable('repositories', type=TableType,
             source=Dependence('.config', depend=get_repository_table))

    def get_config_object(chroot_path):
        from portage.package.ebuild.config import config
        if chroot_path.value == '/':
            with stderr_devnull():
                return config()

    # Объект текущей конфигурации Portage
    Variable('config', source=Dependence('main.cl_chroot_path',
                                         depend=get_config_object))
