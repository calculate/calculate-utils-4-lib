import re
import platform

def get_arch_machine():
    return platform.machine()

def get_arch_gentoo(arch_machine):
    arch = arch_machine.value
    if re.search("i.86", arch):
        return "x86"
    else:
        return {
            "x86_64": "amd64"
        }.get(arch,arch)
