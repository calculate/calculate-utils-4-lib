import os
from calculate.utils.fs import readFile
from calculate.variables.datavars import Variable, Namespace, Dependence, \
    StringType, BooleanType, Calculate
from calculate.utils.files import Process, FilesError
import re


def get_resolution_by_xdpyinfo():
    """
    Get resolution by xdpyinfo utility
    """
    try:
        processXDpy = Process('xdpyinfo')
        if processXDpy.failed():
            return ""
    except (FilesError, OSError):
        return ""
    reRes = re.compile(r"dimensions:\s+(\d+)x(\d+)\s+pixels")
    for line in processXDpy.read_lines():
        searchRes = reRes.search(line)
        if searchRes:
            return "%sx%s" % (searchRes.group(1), searchRes.group(2))
    return ""


def get_resolution_by_xorg():
    pass


def get_standart_resolution():
    # TODO: заглушка
    return "1024x768"


def import_variables():
    with Namespace('resolution'):
        Variable("standard", type=StringType,
                 source=Calculate(get_standart_resolution))
