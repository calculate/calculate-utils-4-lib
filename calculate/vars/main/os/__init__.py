from calculate.variables.datavars import (
    Variable,
    Namespace,
    StringType,
    Calculate,
    Static
    )
from .func import get_arch_machine, get_arch_gentoo
from calculate.utils.system import SystemType

import re
import os
import platform

dictLinuxName = {"CLD": "Calculate Linux Desktop",
                 "CLDX": "Calculate Linux Desktop",
                 "CLDG": "Calculate Linux Desktop",
                 "CDS": "Calculate Directory Server",
                 "CLS": "Calculate Linux Scratch",
                 "CSS": "Calculate Scratch Server",
                 "CMC": "Calculate Media Center",
                 "Gentoo": "Gentoo"}

dictLinuxSubName = {"CLD": "KDE", "CLDX": "XFCE", "CLDG": "GNOME"}

dictNameSystem = {'CDS': 'server',
                  'CLD': 'desktop',
                  'CLDG': 'desktop',
                  'CLDX': 'desktop',
                  'CLS': 'desktop',
                  'CMC': 'desktop',
                  'CSS': 'server'}


def dict_value(d, keyname, fallback_value):
    return d.value.get(keyname.value, fallback_value.value)


def detect_other_shortname(systemroot):
    """Detect other system. Now only Gentoo."""
    gentoo_file = os.path.join(systemroot.value, "etc/gentoo-release")
    if os.path.exists(gentoo_file):
        return "Gentoo"
    return "Linux"


def get_version_from_gentoo_files(systemroot, profilefile):
    """Get version from gentoo files"""
    systemroot = systemroot.value
    gentoo_file = os.path.join(systemroot, "etc/gentoo-release")
    re_ver = re.compile(r"^(\d+\.)*\d+$", re.S)
    if os.path.exists(gentoo_file):
        gentoo_link = self.Get('cl_make_profile')
        if os.path.islink(gentoo_link):
            vers = filter(re_ver.search,
                          os.readlink(gentoo_link).split('/'))
            if vers:
                return vers[-1]


def get_version_from_uname():
    """Get version from uname"""
    re_ver = re.search(r"^(\d+\.)*\d", platform.release(), re.S)
    if re_ver:
        return re_ver.group()


def get_linux_version(systemroot, make_profile):
    return "21"


def import_variables():
    with Namespace("arch"):
        Variable("machine", type=StringType,
                 source=Calculate(get_arch_machine))

        Variable("gentoo", type=StringType,
                 source=Calculate(get_arch_gentoo, ".machine"))

    with Namespace("gentoo"):
        Variable('make_profile', type=StringType,
                 source=Calculate(lambda x: x.value,
                                  "os.gentoo.make_profile"))

    Variable("subsystem", type=StringType,
             source=Calculate(lambda: SystemType.detect()))

    with Namespace("container"):
        Variable("type", type=StringType, source="")

    with Namespace("linux"):
        Variable("shortname", type=StringType,
                 source=Calculate(detect_other_shortname,
                                  "main.cl.chroot_path"))

        Variable("name", type=StringType,
                 source=Calculate(dict_value, dictLinuxName,
                                  ".shortname", Static("Linux")))

        Variable("subname", type=StringType,
                 source=Calculate(dict_value, dictLinuxSubName,
                                  ".shortname", Static("")))

        Variable("system", type=StringType,
                 source=Calculate(dict_value, dictNameSystem,
                                  ".shortname", Static("")))

        Variable("ver", type=StringType,
                 source=Calculate(get_linux_version, "main.cl.chroot_path",
                                  "main.os.gentoo.make_profile"))

        Variable("build", type=StringType, source="")
