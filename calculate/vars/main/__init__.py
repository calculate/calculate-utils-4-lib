from calculate.variables.datavars import (
    Variable,
    StringType,
    ListType
    )
'''
main:
    chroot -> string
    cl_template_path -> string
'''

Variable('cl_chroot_path', type=StringType.readonly, source='/')

Variable('cl_root_path', type=StringType.readonly, source='/')

Variable('cl_template_path', type=ListType.readonly,
         source=['./tests/templates/testfiles/test_runner'])

Variable('cl_ignore_files', type=ListType.readonly,
         source=['*.swp', '.git'])

Variable('cl_config_path', type=StringType.readonly,
         source='/var/lib/calculate/config')

Variable('cl_config_archive', type=StringType.readonly,
         source='/var/lib/calculate/config-archive')

Variable('cl_exec_dir_path', type=StringType.readonly,
         source='/var/lib/calculate/.execute')

Variable('cl_resolutions', type=ListType.readonly,
         source=["1680x1050", "1024x768"])

Variable('cl_image_formats', type=ListType.readonly,
         source=["GIF", "JPG", "JPEG", "PNG", "GFXBOOT"])
