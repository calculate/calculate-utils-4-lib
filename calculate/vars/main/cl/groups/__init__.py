from calculate.variables.datavars import Variable, TableType
from calculate.utils.package import PackageAtomParser

Variable('build', type=TableType, source=[],
         fields=PackageAtomParser.atom_dict_fields)

Variable('uninstall', type=TableType, source=[],
         fields=PackageAtomParser.atom_dict_fields)
