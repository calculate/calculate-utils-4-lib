from calculate.variables.datavars import Variable, ListType, HashType
'''
system:
    env_order -> list
    env_path -> hash
'''

# Список мест, где есть calculate.ini файлы.
Variable('env_order', type=ListType,
         source=[])

# Отображение множества мест, где есть calculate.ini файлы, на пути к ним.
Variable('env_path', type=HashType, source=dict())
