from calculate.variables.datavars import (
    Variable,
    Namespace,
    StringType,
    Calculate,
    Static,
    TableType
    )


def get_repository_table():
    return [{'name': 'gentoo',
             'url':
             'git://git.calculate-linux.org/calculate/gentoo-overlay.git'},
            {'name': 'calculate',
             'url':
             'git://git.calculate-linux.org/calculate/calculate-overlay.git'},
            ]


def import_variables():
    Variable('repositories', type=TableType,
             source=Calculate(get_repository_table))
