from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey


metadata = MetaData()


users_table: Table = Table("users",
                           metadata,
                           Column("id", Integer, primary_key=True),
                           Column("login",
                                  String(20),
                                  unique=True,
                                  nullable=False,
                                  index=True),
                           Column("password",
                                  String(77),
                                  nullable=False)
                           )

rights_table: Table = Table("rights",
                            metadata,
                            Column("id", Integer, primary_key=True),
                            Column("name",
                                   String(20),
                                   unique=True,
                                   nullable=False,
                                   index=True),
                            Column("description", String(40)))

users_rights: Table = Table("users_rights",
                            metadata,
                            Column("user_id", ForeignKey("users.id")),
                            Column("right_id", ForeignKey("rights.id"))
                            )
