from os import environ
from databases import Database


TESTING = bool(environ.get("TESTING", False))


if TESTING:
    DATABASE_URL = "sqlite:///tests/server/testfiles/test.db"
else:
    # Временно.
    DATABASE_URL = "sqlite:///calculate/server/tmp.db"

database = Database(DATABASE_URL)
