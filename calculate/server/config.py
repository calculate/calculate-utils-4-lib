from calculate.logging import dictLogConfig


config = {"socket_path": "./input.sock",
          "variables_path": "calculate/variables",
          "commands_path": "calculate/commands",
          "logger_config": dictLogConfig}
