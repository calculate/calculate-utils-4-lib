from io_module import IOModule
import time


def script_0(io: IOModule, parameters: dict) -> None:
    try:
        io.set_info(f"PARAMETERS: {parameters}")
        value_0 = get_integer(0, parameters, io)
        value_1 = get_integer(1, parameters, io)

        time.sleep(2)
        result = value_0 + value_1

        io.set_info(f"Script executed with result = {result}")
    except Exception as error:
        io.set_error(f"{str(error)}")


def script_1(io: IOModule, parameters: dict) -> None:
    try:
        io.set_info(f"PARAMETERS: {parameters}")
        value_0 = get_integer(0, parameters, io)
        value_1 = get_integer(1, parameters, io)

        time.sleep(3)
        result = value_0 * value_1

        io.set_info(f"Script executed with result = {result}")
    except Exception as error:
        io.set_error(f"{str(error)}")


def script_2(io: IOModule, parameters: dict) -> None:
    try:
        io.set_info(f"PARAMETERS: {parameters}")
        value_0 = get_integer(0, parameters, io)
        value_1 = get_integer(1, parameters, io)

        time.sleep(4)
        result = value_0 ** value_1

        io.set_info(f"Script executed with result = {result}")
    except Exception as error:
        io.set_error(f"{str(error)}")


def get_integer(index: int, parameters: dict, io: IOModule) -> int:
    io.set_info(f"Looking for 'value_{index}'")
    value = parameters.get(f"value_{index}", None)
    if value is None:
        io.set_warning(f"'value_{index}' not found")
        value = io.input(f"Enter 'value_{index}'")
    else:
        io.set_info(f"value_{index} = {value}")
    return int(value)
