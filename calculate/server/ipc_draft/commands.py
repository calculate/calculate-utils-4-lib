from typing import List, Callable, Dict
from scripts import script_0, script_1, script_2
from collections import OrderedDict


class Command:
    '''Тестовый класс команд'''
    def __init__(self,
                 command_id: str,
                 command: str,
                 category: str,
                 title: str,
                 scripts: List[Callable],
                 parameters: Dict[str, dict]):
        self.id = command_id
        self.command = command
        self.category = category
        self.command = command
        self.title = title
        self.scripts = scripts
        self.parameters = parameters


command_0 = Command(command_id="command_0",
                    category="Test",
                    title="Test command number 0",
                    command="cl-command-0",
                    scripts=OrderedDict({"script_0": script_0,
                                         "script_1": script_1}),
                    parameters=[{"group_id": "group_0",
                                 "parameters": [{"id": "value_0",
                                                 "default": 253},
                                                {"id": "value_1",
                                                 "default": 413}]
                                 }
                                ]
                    )


command_1 = Command(command_id="command_1",
                    category="Test",
                    title="Test command number 1",
                    command="cl-command-1",
                    scripts=OrderedDict({"script_0": script_0,
                                         "script_1": script_1,
                                         "script_2": script_2}),
                    parameters=[{"group_id": "group_1",
                                 "parameters": [{"id": "value_0",
                                                 "default": 35},
                                                {"id": "value_1",
                                                 "default": 41}]
                                 },
                                {"group_id": "group_2",
                                 "parameters": [{"id": "value_2",
                                                 "default": 11}]
                                 }
                                ]
                    )


command_2 = Command(command_id="command_2",
                    category="Test",
                    title="Test command number 2",
                    command="cl-command-2",
                    scripts=OrderedDict({"script_0": script_0,
                                         "script_2": script_2}),
                    parameters=[{"group_id": "group_0",
                                 "parameters": [{"id": "value_0",
                                                 "default": 253}]
                                 },
                                {"group_id": "group_1",
                                 "parameters": [{"id": "value_2",
                                                 "default": 413},
                                                {"id": "value_5",
                                                 "default": 23}]
                                 }]
                    )
