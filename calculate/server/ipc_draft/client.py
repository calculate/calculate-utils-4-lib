#! env/bin/python3
import sys
import requests
import logging

from typing import Tuple, List, Union, Dict

from pprint import pprint


LOG_LEVELS = {logging.ERROR: "ERROR",
              logging.INFO: "INFO",
              logging.WARNING: "WARNING",
              logging.DEBUG: "DEBUG",
              logging.CRITICAL: "CRITICAL",
              }


def main():
    # Обращаемся к корню сервера для получения списка доступных ресурсов.
    data, links = get_root_data("http://127.0.0.1:2007")
    if data is None:
        return
    print(f"Connected to {data['name']}")

    print("\nRoot links:")
    pprint(links)

    # Получаем данные о текущей команде.
    cl_command, cl_parameters = get_console_command()
    print_console_parameters(cl_parameters)
    data, links, parameters = find_command(cl_command, links)
    if data is None:
        return
    print_command_info(data)
    print_parameters(parameters)

    links = start_command_configuration(links)

    print("\nConfig links:")
    pprint(links)

    print("\nSetting Parameters")
    print("\nLinks:")
    data, links = set_parameters(links, cl_parameters)
    pprint(links)

    if data is not None:
        print_config_errors(data)

    print("\nCancel configuration:")
    result = cancel_configuration(links)
    pprint(result)


def get_root_data(base_url: str) -> Tuple[Union[dict, None],
                                          Union[dict, None]]:
    try:
        response = requests.get(f"{base_url}/")
        if response.status_code == 200:
            data = response.json()["data"]
            links = response.json()["_links"]

            return data, links
        else:
            detail = response.json()['detail']
            print(f"{response.status_code}: {detail}")
    except Exception as error:
        print("ERROR:", str(error))
    return None, None


def find_command(console_command: str, links: dict) -> dict:
    find_uri = links["find_command"]["href"].format(
                                               console_command=console_command,
                                               is_gui=0)
    response = requests.get(find_uri)

    if response.status_code == 200:
        json = response.json()
        data = json["data"]
        links = json["_links"]
        parameters = json["_embedded"]["parameters"]
        return data, links, parameters
    else:
        detail = response.json()['detail']
        print(f"{response.status_code}: {detail}")
        return None, None, None


def get_parameters(links: dict) -> List[dict]:
    params_uri = links["parameters"]["href"]
    response = requests.get(params_uri)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Response Code: {response.status_code}, {response.text}")
        return None


def start_command_configuration(links: dict) -> Union[dict, None]:
    configure_uri = links["configure"]["href"]
    response = requests.post(configure_uri)
    if response.status_code == 200:
        json = response.json()
        return json["_links"]
    else:
        print(f"Response Code: {response.status_code}, {response.text}")
        return None


def set_parameters(links: dict, parameters: List[dict]) -> Dict[str, dict]:
    configure_uri = links["configure"]["href"]
    response = requests.patch(configure_uri, json=parameters)
    if response.status_code == 200:
        json = response.json()
        data = json.get("data", None)
        links = json["_links"]
        return data, links
    else:
        print(f"Response Code: {response.status_code}, {response.text}")
        return None, None


def cancel_configuration(links: dict) -> List[dict]:
    configure_uri = links["cancel"]["href"]
    response = requests.delete(configure_uri)
    if response.status_code == 200:
        return response.json()
    else:
        print(f"Response Code: {response.status_code}, {response.text}")
        return None


def get_console_command() -> Tuple[str, List[str]]:
    offset = 1 if sys.argv[0].endswith("client.py") else 0
    command_args = []

    if len(sys.argv) < 1 + offset:
        command = None
    else:
        command = sys.argv[offset].split("/")[-1]
        args = sys.argv[1 + offset:]
        if len(args) >= 2 and not (len(args) % 2):
            for index in range(0, len(args) - len(args) % 2, 2):
                id, value = args[index: index + 2]
                command_args.append({"id": id.strip("-"),
                                     "value": value})

    return command, command_args


def print_console_parameters(parameters: dict):
    print("\nConsole parameters:")
    for parameter in parameters:
        print(f"  {parameter['id']} = {parameter['value']}")


def print_command_info(data: dict) -> None:
    print(f"\nCurrent command: {data['id']}\n"
          f"  category: {data['category']}\n"
          f"  title: {data['title']}")


def print_parameters(parameters_data: list) -> None:
    parameters = parameters_data["data"]
    print("\nParameters:")
    for group in parameters:
        print(f"  {group['group_id']}")
        group_parameters = group["parameters"]
        for parameter in group_parameters:
            print(f"    parameter:\t{parameter['id']}\n"
                  f"    default:\t{parameter['default']}\n")


def print_config_errors(errors: Dict[str, str]) -> None:
    print("\nConfiguration errors:")
    for parameter_id, error in errors.items():
        print(f"  {parameter_id}: {error}")


if __name__ == "__main__":
    main()
