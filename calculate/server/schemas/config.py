from pydantic import BaseModel


class ConfigSchema(BaseModel):
    socket_path: str
    variables_path: str
    commands_path: str

    logger_config: dict

    class Config:
        min_any_str_length = 1
        anystr_strip_whitespace = True
