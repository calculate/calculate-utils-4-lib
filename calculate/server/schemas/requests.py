from typing import Any, List
from pydantic import BaseModel


# PATCH /configs/{WID}/parameters
class ParameterValue(BaseModel):
    id: str
    value: Any


class ModifyConfigRequest(BaseModel):
    __root__: List[ParameterValue]


# PUT /configs/{WID}/parameters
class ResetConfigRequest(BaseModel):
    __root__: List[ParameterValue]


# PATCH /executions/{WID}/input
class WriteToExecutionRequest(BaseModel):
    data: Any
