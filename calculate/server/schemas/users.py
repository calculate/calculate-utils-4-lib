from fastapi import HTTPException, status
from pydantic import BaseModel, validator
from typing import List, Optional


class UserCreate(BaseModel):
    login: str
    password: str
    rights: Optional[List[str]] = []


class UserData(UserCreate):
    id: int


class User(BaseModel):
    id: int
    login: str
    rights: List[str]


class UserRead(User):
    @validator("rights")
    def check_permissions(cls, value):
        check_rights("read", value)
        return value


class UserWrite(User):
    @validator("rights")
    def check_permissions(cls, value):
        check_rights("write", value)
        return value


class UserAdmin(User):
    @validator("rights")
    def check_permissions(cls, value):
        check_rights("admin", value)
        return value


def check_rights(right: str, rights_list: List[str]):
    if right not in rights_list:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not enough permissions",
            headers={"WWW-Authenticate": "Bearer"},
        )
