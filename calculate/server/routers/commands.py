from fastapi import APIRouter, Depends
from starlette.requests import Request
from ..utils.responses import (
    ResponseStructure,
    validate_response,
    get_base_url,
    get_command_not_found,
    get_cl_command_not_found,
    )
from typing import Optional

from ..utils.dependencies import get_current_user
from ..utils.users import check_user_rights
from ..server_data import ServerData
from ..schemas.users import User

from ..schemas.responses import (
    GetRootResponse,
    GetCommandsResponse,
    FoundCommandInfo,
    GetCommandParametersResponse,
    )


data = ServerData()
router = APIRouter()


@router.get("/commands", response_model=GetCommandsResponse, tags=["Commands"])
async def get_available_commands(request: Request,
                                 gui: Optional[bool] = False):
    response_data = commands.get_commands(get_base_url(request))
    return validate_response(response_data, GetCommandsResponse,
                             media_type="application/hal+json")


@router.get("/commands/{command}",
            response_model=FoundCommandInfo,
            tags=["Commands"])
async def find_command_data(command: str, request: Request,
                            gui: Optional[bool] = False,
                            by_id: Optional[bool] = False):
    base_url = get_base_url(request)

    if by_id:
        command_data = commands.get_by_id(command, base_url)
        if command_data is None:
            raise get_command_not_found(command)
    else:
        command_data = commands.find_command(command, base_url)
        if command_data is None:
            raise get_cl_command_not_found(command)

    return validate_response(command_data, FoundCommandInfo,
                             media_type="application/hal+json")


@router.get("/commands/{command_id}/parameters",
            response_model=GetCommandParametersResponse,
            tags=["Commands"])
async def get_command_parameters(command_id: str, request: Request):
    parameters_data = commands.get_parameters(command_id,
                                              get_base_url(request))
    if parameters_data is None:
        raise get_command_not_found(command_id)
    return parameters_data
