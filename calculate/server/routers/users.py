from datetime import timedelta

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm

from ..utils.auth import auth_user, create_access_token
from ..utils.dependencies import right_checkers

from ..schemas.tokens import Token
from ..schemas.users import User, UserCreate


ACCESS_TOKEN_EXPIRE_MINUTES = 30
router = APIRouter()


@router.post("/auth", tags=["Authentication"], response_model=Token)
async def authenticate(form_data: OAuth2PasswordRequestForm = Depends()):
    '''Метод обрабатывающий запросы на аутентификацию пользователей по данным
    указанным в форме, возвращает access_token.'''
    user = await auth_user(form_data.username,
                           form_data.password)
    if not user:
        raise HTTPException(
            status_code=400,
            detail=f"User '{form_data.username}' not found"
        )

    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(data={"sub": user.login},
                                       expires_delta=access_token_expires)
    # TODO добавить refresh_token
    return {"access_token": access_token,
            "token_type": "bearer"}


@router.post("/refresh", tags=["Authentication"], response_model=Token)
async def refresh():
    '''Метод для обработки запросов с refresh-токенами.'''
    pass


@router.post("/users/create", tags=["Users management"], response_model=User)
async def create_user(user_info: UserCreate,
                      current_user=Depends(right_checkers["admin"])):
    print("CREATE USER")
