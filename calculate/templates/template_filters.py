def cut(instr: str, field_num: int = 0, delimiter: str = "-") -> str:
    """
    Разделить входную строку `instr` разделительем `delimiter`
    и вернуть поле `field_num`.
    """
    if instr is None:
        return ""
    parts = str(instr).split(delimiter)
    if field_num < len(parts):
        return parts[field_num]
    else:
        return ""
