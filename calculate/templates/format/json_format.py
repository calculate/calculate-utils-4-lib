# vim: fileencoding=utf-8
#
from .base_format import Format
from collections import OrderedDict
import json


class JSONFormat(Format):
    FORMAT: str = 'json'
    EXECUTABLE: bool = False

    comment_symbol: bool = False

    def __init__(self, document_text: str,
                 template_path: str,
                 ignore_comments: bool = False,
                 join_before: bool = False,
                 **kwargs):
        processing_methods: list = []
        super().__init__(processing_methods)
        self._ignore_comments: bool = ignore_comments
        self._join_before: bool = join_before
        self._comments_processing: bool = False

        # Пока что не добавляет заголовок, потому что не очень ясно, как это
        # делать.

        if document_text == '':
            self._document_dictionary: OrderedDict = OrderedDict()
        else:
            self._text_to_dictionary(document_text)

    def _text_to_dictionary(self, json_file_text: str) -> None:
        '''Метод для получения словаря документа, переопределяющий метод
        базового класса.'''
        self._document_dictionary = json.loads(json_file_text,
                                               object_pairs_hook=OrderedDict)

    @property
    def document_text(self):
        '''Метод для получения текста документа, переопределяющий метод
        базового класса.'''
        json_file_text = json.dumps(self._document_dictionary,
                                    ensure_ascii=False,
                                    indent=4).encode('utf8')
        return json_file_text.decode()
