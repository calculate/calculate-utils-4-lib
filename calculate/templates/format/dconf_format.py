# vim: fileencoding=utf-8
#
from ..template_engine import ParametersContainer
from .base_format import Format, FormatError
from calculate.utils.files import Process
import os


class DConfFormat(Format):
    FORMAT = 'dconf'
    EXECUTABLE = True

    def __init__(self, template_text: str,
                 template_path: str,
                 parameters: ParametersContainer = ParametersContainer(),
                 **kwargs):
        self._dconf_text = template_text
        self._init_cmd()

        if parameters.user:
            self._user = parameters.user
        else:
            self._user = "root"

        # Измененные файлы.
        self.changed_files = dict()

        # Предупреждения.
        self._warnings: list = []

    def _init_cmd(self):
        self._dbus_run_session_cmd = "/usr/bin/dbus-run-session"
        if os.path.exists(self._dbus_run_session_cmd):
            self._dbus_run_session_cmd = None
        self._su_cmd = "/usr/bin/su"
        if os.path.exists(self._su_cmd):
            self._su_cmd = None
        self._dconf_cmd = "/usr/bin/dconf"
        if os.path.exists(self._dconf_cmd):
            self._dconf_cmd = None

    def execute_format(self, target_path: str,
                       chroot_path: str = '/') -> dict:
        if None in (self._dbus_run_session_cmd, self._su_cmd, self._dconf_cmd):
            raise FormatError("the 'dconf' format is not available.")

        if not os.path.exists(os.path.dirname(target_path)):
            raise FormatError(
                    "dconf base directory does not exist: {}".format(
                                                 os.path.dirname(target_path)))
        if os.path.isdir(target_path):
            base_directory = target_path
        else:
            base_directory = os.path.dirname(target_path)

        dconf_command = f"{0} {1} load {2}".format(self._dbus_run_session_cmd,
                                                   self._dconf_cmd,
                                                   base_directory)
        command = [self._su_cmd, self._user, "-c", dconf_command]
        dconf_process = Process(*command)
        dconf_process.write(self._dconf_text)

        if dconf_process.success():
            return self.changed_files
        else:
            error = dconf_process.read_error()
            raise FormatError(error)

        return self.changed_files

    def __bool__(self):
        return bool(self._patch_text)

    @property
    def warnings(self):
        return self._warnings
