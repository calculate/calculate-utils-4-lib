# vim: fileencoding=utf-8
#
from .base_format import Format
from ..template_engine import ParametersContainer


class RawFormat(Format):
    '''Класс формата raw.'''
    FORMAT = 'raw'
    EXECUTABLE = False
    FORMAT_PARAMETERS = {'comment'}

    def __init__(self, document_text: str,
                 template_path,
                 ignore_comments=False,
                 join_before=False,
                 add_header=False,
                 already_changed=False,
                 parameters=ParametersContainer(),
                 **kwargs):
        self.comment_symbol = parameters.comment or ""
        self._before = join_before

        self.header, self._document_text, self.shebang =\
            self._get_header_and_document_text(
                                           document_text,
                                           template_path,
                                           already_changed=already_changed,
                                           check_shebang=True)

    @property
    def document_text(self):
        return '{}{}{}\n'.format(self.shebang, self.header,
                                 self._document_text)

    def join_template(self, template):
        sep = ""
        if self._before:
            if (template._document_text
               and not template._document_text.endswith("\n")):
                sep = "\n"
            self._document_text = '{0}{1}{2}'.format(
                                                 template._document_text, sep,
                                                 self._document_text)
        else:
            if self._document_text and not self._document_text.endswith("\n"):
                sep = "\n"
            self._document_text = '{0}{1}{2}'.format(
                                                 self._document_text, sep,
                                                 template._document_text)
        if template.shebang:
            self.shebang = template.shebang

    def __bool__(self):
        return bool(self.document_text.strip())
