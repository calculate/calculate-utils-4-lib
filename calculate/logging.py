from logging import DEBUG

dictLogConfig = {
    "version": 1,
    "formatters": {
        "form_1": {
            "format":
            '%(asctime)s %(levelname)s: %(message)s'
            }
        },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "formatter": "form_1",
            "level": DEBUG
            }
        },
    "root": {
        "handlers": ['console'],
        "level": DEBUG
        }
}
