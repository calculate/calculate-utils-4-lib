#! /usr/bin/python3
import argparse

from calculate.templates.template_processor import DirectoryProcessor

from calculate.variables.loader import Datavars

from calculate.utils.io_module import IOModule
from calculate.utils.package import NonePackage
from calculate.utils.tools import flat_iterable


def main():
    parser = argparse.ArgumentParser('Run templates.')
    parser.add_argument('-a', '--action', action='append', type=str, nargs='+',
                        help="action parameter value.")
    parser.add_argument('-i', '--install', type=str,
                        help="atom name of a target package.")
    parser.add_argument('--dbpkg', action='store_true',
                        help=("flag for switching template engine's mode from"
                              " standard mode to the mode allowing changing of"
                              "CONTENTS files."))
    parser.add_argument('-b', '--build', type=str,
                        help="atom name of a build package.")
    parser.add_argument('-u', '--uninstall', type=str,
                        help="atom name of a uninstalling package.")

    args = parser.parse_args()

    datavars = Datavars()
    io_module = IOModule()

    if args.install is None:
        install = NonePackage
    elif args.install.strip().casefold() == 'all':
        install = None
    else:
        install = args.install

    group_packages = {}
    if args.build is not None:
        group_packages['build'] = args.build
    if args.uninstall is not None:
        group_packages['uninstall'] = args.uninstall

    action = list(flat_iterable(args.action))
    template_processor = DirectoryProcessor(action,
                                            datavars_module=datavars,
                                            install=install,
                                            output_module=io_module,
                                            dbpkg=args.dbpkg,
                                            **group_packages)
    template_processor.process_template_directories()
    datavars.save_variables()


if __name__ == "__main__":
    main()
