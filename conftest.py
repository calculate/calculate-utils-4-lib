import os
import pytest

from collections import OrderedDict
os.environ["TESTING"] = "True"


@pytest.fixture(scope='function')
def StringDictionaries():
    ParamLine1 = OrderedDict({'parameter name1': 'value1'})
    ParamLine2 = OrderedDict({'parameter name2': 'value2'})
    ParamLine3 = OrderedDict({'parameter name3': 'value3'})

    Section1 = OrderedDict({'section name1': OrderedDict(**ParamLine1,
                                                         **ParamLine2)})
    Section2 = OrderedDict({'section name2': OrderedDict(**ParamLine1,
                                                         **ParamLine2)})
    Section3 = OrderedDict({'section name3': OrderedDict(**ParamLine3)})

    OriginalDictionary = OrderedDict(**Section1, **Section2)
    TemplateDictionary = OrderedDict(**Section3)
    ResultDictionary = OrderedDict(**Section1, **Section2, **Section3)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def TupleDictionaries():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})
    ParamLine3 = OrderedDict({('', 'parameter name3'): 'value3'})

    Section1 = OrderedDict({('', 'section name1'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2 = OrderedDict({('', 'section name2'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section3 = OrderedDict({('', 'section name3'): OrderedDict(**ParamLine3)})

    OriginalDictionary = OrderedDict(**Section1, **Section2)
    TemplateDictionary = OrderedDict(**Section3)
    ResultDictionary = OrderedDict(**Section1, **Section2, **Section3)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def MergeSectionDictionaries():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})
    ParamLine3 = OrderedDict({('', 'parameter name3'): 'value3'})

    Section1 = OrderedDict({('', 'section name1'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2 = OrderedDict({('', 'section name2'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2Add = OrderedDict({('', 'section name2'):
                              OrderedDict(**ParamLine3)})
    Section2Added = OrderedDict({('', 'section name2'):
                                OrderedDict(**ParamLine1,
                                            **ParamLine2,
                                            **ParamLine3)})

    OriginalDictionary = OrderedDict(**Section1, **Section2)
    TemplateDictionary = OrderedDict(**Section2Add)
    ResultDictionary = OrderedDict(**Section1, **Section2Added)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def ChangeParameterDictionaries():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})
    ParamLine3 = OrderedDict({('', 'parameter name3'): 'value3'})
    ParamLine2New = OrderedDict({('', 'parameter name2'): 'NewValue'})
    ParamLine3New = OrderedDict({('', 'parameter name3'): 'OtherValue'})

    Section = OrderedDict({('', 'section name1'): OrderedDict(**ParamLine1,
                                                              **ParamLine2,
                                                              **ParamLine3)})
    SectionNew = OrderedDict({('', 'section name1'):
                             OrderedDict(**ParamLine1,
                                         **ParamLine2New,
                                         **ParamLine3New)})

    OriginalDictionary = OrderedDict(**Section)
    TemplateDictionary = OrderedDict(**SectionNew)
    ResultDictionary = TemplateDictionary

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def DeleteSectionDictionaries():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})

    Section1 = OrderedDict({('', 'section name1'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2 = OrderedDict({('', 'section name2'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    SectionDel = OrderedDict({('!', 'section name2'): OrderedDict()})

    OriginalDictionary = OrderedDict(**Section1, **Section2)
    TemplateDictionary = OrderedDict(**SectionDel)
    ResultDictionary = OrderedDict(**Section1)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def ReplaceSectionDictionaries():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})
    ParamLine3 = OrderedDict({('', 'parameter name3'): 'value3'})
    ParamLine4 = OrderedDict({('', 'parameter name4'): 'value4'})

    Section1 = OrderedDict({('', 'section name1'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2 = OrderedDict({('', 'section name2'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2Replace = OrderedDict({('-', 'section name2'):
                                  OrderedDict(**ParamLine3, **ParamLine4)})
    Section2Replaced = OrderedDict({('', 'section name2'):
                                   OrderedDict(**ParamLine3, **ParamLine4)})

    OriginalDictionary = OrderedDict(**Section1, **Section2)
    TemplateDictionary = OrderedDict(**Section2Replace)
    ResultDictionary = OrderedDict(**Section1, **Section2Replaced)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def DeleteParameterDictionaries():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})
    ParamLineDel = OrderedDict({('!', 'parameter name2'): 'value2'})

    Section1 = OrderedDict({('', 'section name1'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    Section2 = OrderedDict({('', 'section name2'): OrderedDict(**ParamLine1,
                                                               **ParamLine2)})
    SectionDel = OrderedDict({('', 'section name2'):
                             OrderedDict(**ParamLineDel)})
    SectionAfterDel = OrderedDict({('', 'section name2'):
                                  OrderedDict(**ParamLine1)})

    OriginalDictionary = OrderedDict(**Section1, **Section2)
    TemplateDictionary = OrderedDict(**SectionDel)
    ResultDictionary = OrderedDict(**Section1, **SectionAfterDel)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


@pytest.fixture(scope='function')
def DictionariesWithoutSections():
    ParamLine1 = OrderedDict({('', 'parameter name1'): 'value1'})
    ParamLine2 = OrderedDict({('', 'parameter name2'): 'value2'})
    ParamLine3 = OrderedDict({('', 'parameter name3'): 'value3'})
    ParamLineDel = OrderedDict({('!', 'parameter name2'): ''})

    OriginalDictionary = OrderedDict(**ParamLine1, **ParamLine2)
    TemplateDictionary = OrderedDict(**ParamLine3, **ParamLineDel)
    ResultDictionary = OrderedDict(**ParamLine1, **ParamLine3)

    return (OriginalDictionary, TemplateDictionary, ResultDictionary)


def pytest_addoption(parser):
    parser.addoption(
        "--chroot-test",
        action="store_true",
        default=False,
        help="run chroot tests"
    )
    parser.addoption(
        "--backgrounds-slow",
        action="store_true",
        default=False,
        help="run slow backgrounds tests."
    )
    parser.addoption(
        "--from-root",
        action="store_true",
        default=False,
        help="run tests that needs root rights."
    )


def pytest_collection_modifyitems(config, items):
    if not config.getoption("--chroot-test"):
        skip_chroot = pytest.mark.skip(
                          reason="need --chroot-test option to run")
        for item in items:
            if "chroot" in item.keywords:
                item.add_marker(skip_chroot)

    if not config.getoption("--backgrounds-slow"):
        skip_backgrounds = pytest.mark.skip(
                                reason="need --backgrounds-slow option to run")
        for item in items:
            if "backgrounds_slow" in item.keywords:
                item.add_marker(skip_backgrounds)

    if not config.getoption("--from-root"):
        skip_from_root = pytest.mark.skip(
                                reason="need --from-root option to run")
        for item in items:
            if "needs_root" in item.keywords:
                item.add_marker(skip_from_root)
