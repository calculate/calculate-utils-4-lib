import os
import pytest
import shutil
from fastapi.testclient import TestClient
from calculate.server.server import app


TESTFILES_PATH = os.path.join(os.getcwd(), 'tests/server/testfiles')

test_client = TestClient(app)


def authenticate(username: str, password: str):
    request_headers = {"accept": "application/json",
                       "Content-Type": "application/x-www-form-urlencoded"}
    request_data = {"username": username,
                    "password": password,
                    "grant_type": "password",
                    "scope": None,
                    "client_id": None}
    token = test_client.post("/auth", data=request_data, json=request_headers)
    token_header = token.json()
    access_token = token_header["access_token"]
    return {"accept": "application/json",
            "Authorization": f"Bearer {access_token}"}


@pytest.mark.server
def test_to_make_testfiles():
    shutil.copytree(os.path.join(TESTFILES_PATH, 'var.backup'),
                    os.path.join(TESTFILES_PATH, 'var'),
                    symlinks=True)
    shutil.copytree(os.path.join(TESTFILES_PATH, 'etc.backup'),
                    os.path.join(TESTFILES_PATH, 'etc'),
                    symlinks=True)


# @pytest.mark.server
# def test_get_root_message():
#     authorization_headers = authenticate("denis", "secret")
#     response = test_client.get("/", headers=authorization_headers)
#     assert response.status_code == 200
#     assert response.json() == {"msg": "root msg"}


# @pytest.mark.server
# def test_get_commands_list():
#     authorization_headers = authenticate("denis", "secret")
#     response = test_client.get("/commands", headers=authorization_headers)
#     assert response.status_code == 200
#     assert response.json() == {"test_1":
#                                {"title": "Test 1",
#                                 "category": "Test Category",
#                                 "icon": "/path/to/icon_1.png",
#                                 "command": "test_1"},
#                                "test_2":
#                                {"title": "Test 2",
#                                 "category": "Test Category",
#                                 "icon": "/path/to/icon_2.png",
#                                 "command": "cl_test_2"}}


# @pytest.mark.server
# def test_post_command():
#     response = test_client.get("/commands/")
#     assert response.status_code == 200


# @pytest.mark.server
# def test_get_command_by_cid():
#     response = test_client.get("/commands/0")
#     assert response.status_code == 200
#     assert response.json() == {"id": 0, "name": "command_0"}


# @pytest.mark.server
# def test_get_worker_message_by_wid():
#     response = test_client.get("/workers/0")
#     assert response.status_code == 200
#     data = response.json()
#     assert data == {'type': 'log', 'level': 'INFO',
#                     'msg': 'recieved message INFO'}


@pytest.mark.server
def test_for_removing_testfiles():
    shutil.rmtree(os.path.join(TESTFILES_PATH, 'var'))
    shutil.rmtree(os.path.join(TESTFILES_PATH, 'etc'))
