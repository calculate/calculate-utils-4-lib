import os
from calculate.logging import dictLogConfig


testfiles_path = os.path.join(os.getcwd(), 'tests/server/testfiles')


config = {"socket_path": "./input.sock",
          "variables_path": 'tests/server/testfiles/variables',
          "commands_path": 'tests/server/testfiles/commands',
          "logger_config": dictLogConfig}
