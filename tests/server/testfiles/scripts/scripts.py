from calculate.scripts.scripts import Script, Task


def action_1(output, arg1: str) -> str:
    '''Задача выполняемая скриптом ниже.'''
    return f'os.calculate = {arg1}'


# Тестовый скрипт.
script_1 = Script('script_1'
                  ).tasks(Task(id='task_1',
                               name="Task 1",
                               action=action_1,
                               args=["os.calculate"])
                          )

# Тестовый скрипт.
script_2 = Script('script_2'
                  ).tasks(Task(id='task_2',
                               name="Task 2",
                               action=action_1,
                               args=["os.calculate"]))
