from calculate.parameters.parameters import BaseParameter, Integer,\
                                            String, ValidationError


class Parameter1(BaseParameter):
    type = Integer()

    def bind_method(self, var):
        return var.value, None

    def validate(self, var):
        if var.value < 10:
            raise ValidationError("The value must be greater than 10")


class Parameter2(BaseParameter):
    type = String()

    def bind_method(self):
        return '/var/lib/calculate', None

    def validate(self, value):
        if not value.startswith('/var/lib'):
            raise ValidationError("The value must starts with a"
                                  " '/var/lib'")
