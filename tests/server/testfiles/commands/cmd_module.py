from calculate.parameters.parameters import Description
from calculate.commands.commands import Command
from ..parameters.parameters import Parameter1, Parameter2
from ..scripts.scripts import script_1, script_2


command_1 = Command(command_id='test_1',
                    category='Test Category',
                    title='Test 1',
                    command="test_1",
                    script=script_1,
                    namespace='os',
                    icon="/path/to/icon_1.png",
                    rights=['group'],
                    parameters=[
                        Parameter1
                        (
                          'first-param', 'Test',
                          Description(
                            short='First parameter',
                            full='First parameter, but who cares'),
                          shortname='f'
                        ).bind('os.linux.test_5'),
                        Parameter2
                        (
                          'second-param', 'Test',
                          Description(
                            short='Second parameter'),
                          shortname='s'
                        )])

command_2 = Command(command_id='test_2',
                    category='Test Category',
                    title='Test 2',
                    script=script_2,
                    namespace='os',
                    icon="/path/to/icon_2.png",
                    rights=['group'],
                    parameters=[
                        Parameter1
                        (
                          'first-param', 'Test',
                          Description(
                            short='First parameter',
                            full='First parameter, but who cares'),
                          shortname='f'
                        ).bind('os.linux.test_5'),
                        Parameter2
                        (
                          'second-param', 'Test',
                          Description(
                            short='Second parameter'),
                          shortname='s'
                        )])
