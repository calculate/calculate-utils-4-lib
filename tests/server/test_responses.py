import pytest
from calculate.server.utils.responses import ResponseStructure


@pytest.mark.responses
@pytest.mark.parametrize('case', [
        {
            "name": "One positional argument",
            "args": ["Very important data"],
            "kwargs": {},
            "result": {"data": "Very important data"}
        },
        {
            "name": "multiple positional args",
            "args": [1, 2, 3],
            "kwargs": {},
            "result": {"data": [1, 2, 3]}
        },
        {
            "name": "only keyword args",
            "args": [],
            "kwargs": {"name": "Calculate Server",
                       "version": "0.1"},
            "result": {"data": {"name": "Calculate Server",
                                "version": "0.1"}}
        },
        {
            "name": "Both positional and keyword args",
            "args": [1, 2, 3],
            "kwargs": {"name": "Calculate Server",
                       "version": "0.1"},
            "result": {"data": [1, 2, 3]}
        },
    ],
    ids=lambda case: case["name"])
def test_creation_response_dictionary_using_only_add_data_method(case):
    response = ResponseStructure("http://0.0.0.0:2007/")
    response.add_data(*case["args"], **case["kwargs"])
    assert response.get_dict() == case["result"]


@pytest.mark.responses
@pytest.mark.parametrize('case', [
        {
            "name": "Two calls with positional values",
            "data": [
                {"args": ["Very important data 1"],
                 "kwargs": {}},
                {"args": ["Very important data 2"],
                 "kwargs": {}},
                    ],
            "result": {"data": ["Very important data 1",
                                "Very important data 2"]
                       },
        },
        {
            "name": "Call with positional argument and with keywords one",
            "data": [
                {"args": ["Very important data"],
                 "kwargs": {}},
                {"args": [],
                 "kwargs": {"name": "Calculate Server",
                            "version": "0.1"}},
                    ],
            "result": {"data": {"name": "Calculate Server",
                                "version": "0.1"}
                       },
        },
        {
            "name": "Call with keyword arguments and with positional one",
            "data": [
                {"args": [],
                 "kwargs": {"name": "Calculate Server",
                            "version": "0.1"}},
                {"args": ["Very important data"],
                 "kwargs": {}},
                    ],
            "result": {"data": "Very important data"},
        },
        {
            "name": "Two calls with keyword arguments",
            "data": [
                {"args": [],
                 "kwargs": {"name": "Calculate Server"}},
                {"args": [],
                 "kwargs": {"version": "0.1"}},
                    ],
            "result": {"data": {"name": "Calculate Server",
                                "version": "0.1"}
                       },
        },
    ],
    ids=lambda case: case["name"])
def test_multiple_add_data_method_callings(case):
    response = ResponseStructure("http://0.0.0.0:2007/")
    for data in case["data"]:
        response.add_data(*data["args"], **data["kwargs"])
    assert response.get_dict() == case["result"]


@pytest.mark.responses
@pytest.mark.parametrize('case', [
        {
            "name": "Passing not templated uri",
            "action": "commands",
            "uri": "/commands/",
            "templated": False,
            "result": {
                "_links": {
                    "commands": {
                        "href": "http://0.0.0.0:2007/commands"
                    }
                }
            }
        },
        {
            "name": "Passing templated uri",
            "action": "find_command",
            "uri": "commands/{cl_command}",
            "templated": True,
            "result": {
                "_links": {
                     "find_command": {
                           "href": "http://0.0.0.0:2007/commands/{cl_command}",
                           "templated": True
                     }
                }
            }
        },
    ],
    ids=lambda case: case["name"])
def test_creation_response_dictionary_using_only_add_links_method(case):
    response = ResponseStructure("http://0.0.0.0:2007/")
    response.add_link(case["action"], case["uri"], case["templated"])
    assert response.get_dict() == case["result"]


@pytest.mark.responses
@pytest.mark.parametrize('case', [
        {
          "name": "One embed method call",
          "embed": {
            "menu":
            (ResponseStructure("http://0.0.0.0:2007/").
             add_data(name="Calculate Server", version="0.1").
             add_link("get_commands", "commands").
             add_link("find_command", "commands/{cl_command}",
                      templated=True)
             ).get_dict(),
          },
          "result": {
            "_embedded": {
              "menu": {
                "data": {
                  "name": "Calculate Server",
                  "version": "0.1"
                },
                "_links": {
                  "get_commands": {
                    "href": "http://0.0.0.0:2007/commands"
                  },
                  "find_command": {
                    "href": "http://0.0.0.0:2007/commands/{cl_command}",
                    "templated": True
                  },
                }
              }
            }
          }
        },
        {
          "name": "Two embed method calls",
          "embed": {
            "first":
            (ResponseStructure("http://0.0.0.0:2007/").
             add_data(id=1, name="first", description="First value").
             add_link("self", "values/1").
             add_link("find_value", "values/search/{value_name}",
                      templated=True)
             ).get_dict(),
            "second":
            (ResponseStructure("http://0.0.0.0:2007/").
             add_data(id=2, name="second", description="Second value").
             add_link("self", "values/2").
             add_link("find_value", "values/search/{value_name}",
                      templated=True)
             ).get_dict(),
          },
          "result": {
            "_embedded": {
              "first": {
                "data": {
                  "id": 1,
                  "name": "first",
                  "description": "First value",
                },
                "_links": {
                  "self": {
                    "href": "http://0.0.0.0:2007/values/1"
                  },
                  "find_value": {
                    "href": "http://0.0.0.0:2007/values/search/{value_name}",
                    "templated": True
                  },
                }
              },
              "second": {
                "data": {
                  "id": 2,
                  "name": "second",
                  "description": "Second value",
                },
                "_links": {
                  "self": {
                    "href": "http://0.0.0.0:2007/values/2"
                  },
                  "find_value": {
                    "href": "http://0.0.0.0:2007/values/search/{value_name}",
                    "templated": True
                  },
                }
              }
            }
          }
        },
    ],
    ids=lambda case: case["name"])
def test_creation_response_dictionary_using_only_embed_method(case):
    response = ResponseStructure("http://0.0.0.0:2007/")
    for embed_name, embed_data in case["embed"].items():
        response.embed(embed_name, embed_data)
    assert response.get_dict() == case["result"]
