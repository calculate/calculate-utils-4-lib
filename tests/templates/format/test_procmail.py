import pytest
from collections import OrderedDict
from calculate.templates.format.procmail_format import ProcmailFormat


@pytest.mark.formats
@pytest.mark.procmail
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''
        net.ipv4.ip_forward = 0
        parameter_name = /home/divanov/Home
        other_parameter = yes'''

        result = OrderedDict({('', 'net.ipv4.ip_forward'): ['0'],
                              ('', 'parameter_name'): ['/home/divanov/Home'],
                              ('', 'other_parameter'): ['yes']})

        procmail_object = ProcmailFormat(document_text, '/path/to/template')
        assert procmail_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        net.ipv4.ip_forward = 0


        parameter_name = /home/divanov/Home

        other_parameter = yes'''

        result = OrderedDict({('', 'net.ipv4.ip_forward'): ['0'],
                              ('', 'parameter_name'): ['/home/divanov/Home'],
                              ('', 'other_parameter'): ['yes']})

        procmail_object = ProcmailFormat(document_text, '/path/to/template')
        assert procmail_object._document_dictionary == result

    def test_if_input_document_contains_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''
        !net.ipv4.ip_forward = 0


        local_recipient_maps = ldap:/etc/postfix/ldap-recipient.cf, ldap:/etc/postfix/ldap-recipient-gr.cf, ldap:/etc/postfix/ldap-recipient-repl.cf
        parameter_name = /home/divanov/Home

        -other_parameter = yes'''

        result = OrderedDict({('!', 'net.ipv4.ip_forward'): ['0'],
                              ('', 'local_recipient_maps'):
                              ['ldap:/etc/postfix/ldap-recipient.cf, ldap:/etc/postfix/ldap-recipient-gr.cf, ldap:/etc/postfix/ldap-recipient-repl.cf'],
                              ('', 'parameter_name'): ['/home/divanov/Home'],
                              ('-', 'other_parameter'): ['yes']})

        procmail_object = ProcmailFormat(document_text, '/path/to/template')
        assert procmail_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''
        # Comment1
        net.ipv4.ip_forward = 0


        # Comment2
        # Comment3
        parameter_name = /home/divanov/Home

        # Comment
        !other_parameter = yes'''

        result = OrderedDict({('', 'net.ipv4.ip_forward'): ['# Comment1', '0'],
                              ('', 'parameter_name'): ['# Comment2',
                                                       '# Comment3',
                                                       '/home/divanov/Home'],
                              ('!', 'other_parameter'): ['# Comment',
                                                         'yes']})

        procmail_object = ProcmailFormat(document_text, '/path/to/template')
        assert procmail_object._document_dictionary == result

    def test_if_the_IgnoreComments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''
        # Comment1
        net.ipv4.ip_forward = 0


        # Comment2
        # Comment3
        parameter_name = /home/divanov/Home

        # Comment
        !other_parameter = yes'''

        result = OrderedDict({('', 'net.ipv4.ip_forward'): ['0'],
                              ('', 'parameter_name'): ['/home/divanov/Home'],
                              ('!', 'other_parameter'): ['yes']})

        procmail_object = ProcmailFormat(document_text, '/path/to/template',
                                         ignore_comments=True)
        assert procmail_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''
        !net.ipv4.ip_forward = 0


        !parameter_name = 

        !other_parameter'''

        result = OrderedDict({('!', 'net.ipv4.ip_forward'): ['0'],
                              ('!', 'parameter_name'): [],
                              ('!', 'other_parameter'): []})

        procmail_object = ProcmailFormat(document_text, '/path/to/template')
        assert procmail_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/procmail/original',
                  'r') as original_file:
            original_text = original_file.read()
            procmail_original_object = ProcmailFormat(original_text,
                                                      '/path/to/template')

        with open('./tests/templates/format/testfiles/procmail/template',
                  'r') as template_file:
            template_text = template_file.read()
            procmail_template_object = ProcmailFormat(template_text,
                                                      '/path/to/template',
                                                      ignore_comments=True)

        procmail_original_object.join_template(procmail_template_object)

        with open('./tests/templates/format/testfiles/procmail/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert procmail_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''# port for HTTP (descriptions, SOAP, media transfer) traffic
port=8200

# specify the user account name or uid to run as
user=jmaggard

friendly_name=mike-desktop

vm.dirty_ratio = 3'''

        document_2 = '''# port for HTTP (descriptions, SOAP, media transfer) traffic
port=8200

# specify the user account name or uid to run as
user=lol
vm.dirty_ratio=4
media_dir=PV,/var/calculate/server-data/samba/share
net.ipv4.icmp_echo_ignore_broadcasts=1
'''

        document_1_object = ProcmailFormat(document_1, '/path/to/template')
        document_2_object = ProcmailFormat(document_2, '/path/to/template')

        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
net.ipv4.ip_forward = 0
parameter_name = /home/divanov/Home
other_parameter = yes
'''

        template_text = '''
!parameter_name = /home/divanov/Home
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
net.ipv4.ip_forward=0
other_parameter=yes
'''

        original_object = ProcmailFormat(original_text, '/path/to/template',
                                         add_header=True)
        template_object = ProcmailFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
net.ipv4.ip_forward = 0
parameter_name = /home/divanov/Home
other_parameter = yes
'''

        template_text = '''
!parameter_name = /home/divanov/Home
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
net.ipv4.ip_forward=0
other_parameter=yes
'''

        original_object = ProcmailFormat(original_text, '/path/to/template',
                                         add_header=True)
        template_object = ProcmailFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
net.ipv4.ip_forward = 0
parameter_name = /home/divanov/Home
other_parameter = yes
'''

        template_text = '''
!parameter_name = /home/divanov/Home
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
net.ipv4.ip_forward=0
other_parameter=yes
'''

        original_object = ProcmailFormat(original_text, '/path/to/template',
                                         add_header=True, already_changed=True)
        template_object = ProcmailFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
