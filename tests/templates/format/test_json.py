import pytest
from collections import OrderedDict
from calculate.templates.format.json_format import JSONFormat


@pytest.mark.formats
@pytest.mark.json
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameters_and_parameter_blocks__the_initialised_object_contains_correct_dictionary(self):
        documentText = '''
        {
            "Param1":"ParamValue1",
            "Param2": 1,
            "BlockParam1":{
                "BlockParam1":1,
                "BlockParam2":0
            },
            "Param3": true
        }
        '''

        blockContent = OrderedDict({"BlockParam1": 1, "BlockParam2": 0})
        result = OrderedDict({'Param1': 'ParamValue1',
                              'Param2': 1,
                              'BlockParam1': blockContent,
                              'Param3': True})

        jsonObject = JSONFormat(documentText, '/path/to/template')
        assert jsonObject._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/json/'
                  'original.json', 'r') as originalFile:
            originalText = originalFile.read()
            jsonOriginalObject = JSONFormat(originalText, '/path/to/template')

        with open('./tests/templates/format/testfiles/json/'
                  'template.json', 'r') as templateFile:
            templateText = templateFile.read()
            jsonTemplateObject = JSONFormat(templateText, '/path/to/template')

        jsonOriginalObject.join_template(jsonTemplateObject)

        with open('./tests/templates/format/testfiles/json/'
                  'result.json', 'r') as resultFile:
            resultText = resultFile.read()

        assert jsonOriginalObject.document_text == resultText

    def test_make_template(self):
        document_1 = '''
        {
            "Param1":"ParamValue1",
            "Param2": 1,
            "BlockParam1":{
                "BlockParam1":1,
                "BlockParam2":0
            },
            "BlockParam2":{
                "BlockParam1":"value",
                "BlockParam2":false,
                "BlockParam4":"other value"
            },
            "Param3": true
        }
        '''

        document_2 = '''{
    "Param1": "ParamValue1",
    "BlockParam1": {
        "BlockParam1": 12,
        "BlockParam3": true
    },
    "BlockParam2": {
        "BlockParam2": true,
        "BlockParam4": "other value",
        "BlockParam3": 12
    },
    "Param3": false,
    "Param4": 12.3
}'''

        document_1_object = JSONFormat(document_1, '/path/to/template')
        document_2_object = JSONFormat(document_2, '/path/to/template')
        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_json_join_with_unicode_symbols(self):
        original_text = '''{
    "Description[cs]": "Upravit hlasitost zařízení a aplikací",
    "Description[da]": "Justér lydstyrke for enheder og programmer",
    "Description[de]": "Anpassung der Lautstärke von Geräten und Anwendungen",
    "Description[el]": "Προσαρμογή της έντασης των συσκευών και εφαρμογών",
    "test": "true"
}
'''

        template_text = '''{
"test": "false"
}
'''

        join_result = '''{
    "Description[cs]": "Upravit hlasitost zařízení a aplikací",
    "Description[da]": "Justér lydstyrke for enheder og programmer",
    "Description[de]": "Anpassung der Lautstärke von Geräten und Anwendungen",
    "Description[el]": "Προσαρμογή της έντασης των συσκευών και εφαρμογών",
    "test": "false"
}'''

        original_object = JSONFormat(original_text, '/path/to/template')
        template_object = JSONFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
