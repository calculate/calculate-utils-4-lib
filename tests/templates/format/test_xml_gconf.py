import pytest
from collections import OrderedDict
from calculate.templates.format.xml_gconf_format import XMLGConfFormat


@pytest.mark.formats
@pytest.mark.xml_gconf
class TestParsingMethods:
    def test_if_input_document_is_simple_gconf_tree__the_format_object_contains_correct_dictionary(self):
        document_text = '''<?xml version="1.0" encoding="UTF-8"?>
        <gconf>
            <dir name="gnome">
                <dir name="ymsgr">
                    <entry name="needs_terminal">
                        <default type="string">
                            <stringvalue>Something I can never have.</stringvalue>
                        </default>
                        <local_schema short_desc="Run the command in a terminal">
                            <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
                        </local_schema>
                    </entry>
                    <entry name="logo_icon_name" mtime="1342486180" type="string">
                        <stringvalue>distributor</stringvalue>
                    </entry>
                </dir>
            </dir>
        </gconf>
        '''

        result = OrderedDict({('', 'gconf'):
                 OrderedDict({('', 'dir', ('name', 'gnome')):
                 OrderedDict({('', 'dir', ('name', 'ymsgr')):
                 OrderedDict({('', 'entry', ('name', 'needs_terminal')):
                 OrderedDict({('', 'default', ('type', 'string')):
                 OrderedDict({('', 'stringvalue'): 'Something I can never have.'}),
                 ('', 'local_schema', ('short_desc', 'Run the command in a terminal')):
                 OrderedDict({('', 'longdesc'): 'True if the command used to handle this type of URL should be run in a terminal.'})}),
                 ('', 'entry', ('name', 'logo_icon_name'), ('mtime', '1342486180'), ('type', 'string')):
                 OrderedDict({('', 'stringvalue'): 'distributor'})})})})})

        xml_gconf_object = XMLGConfFormat(document_text, '/path/to/template')
        assert xml_gconf_object._document_dictionary == result

    def test_if_input_document_is_simple_gconf__the_format_object_contains_correct_dictionary(self):
        document_text = '''<?xml version="1.0" encoding="UTF-8"?>
        <gconf>
            <entry name="options" mtime="1298136657" type="list" ltype="string">
                <li type="string">
                    <stringvalue>grp grp:lwin_toggle</stringvalue>
                </li>
            </entry>
            <entry name="layouts" mtime="1298136657" type="list" ltype="string">
                <li type="string">
                    <stringvalue>us</stringvalue>
                </li>
                <li type="string">
                    <stringvalue>ru</stringvalue>
                </li>
            </entry>
            <entry name="hinting" mtime="1298136657" type="list">
                <stringvalue>full</stringvalue>
            </entry>
        </gconf>
        '''

        result = OrderedDict({('', 'gconf'):
                              OrderedDict({('', 'entry',
                                            ('name', 'options'),
                                            ('mtime', '1298136657'),
                                            ('type', 'list'),
                                            ('ltype', 'string')):
                                                ['grp grp:lwin_toggle'],
                                           ('', 'entry',
                                            ('name', 'layouts'),
                                            ('mtime', '1298136657'),
                                            ('type', 'list'),
                                            ('ltype', 'string')):
                                                ['us', 'ru'],
                                           ('', 'entry',
                                            ('name', 'hinting'),
                                            ('mtime', '1298136657'),
                                            ('type', 'list')):
                                                OrderedDict(
                                                    {('', 'stringvalue'):
                                                        'full'})})})

        xml_gconf_object = XMLGConfFormat(document_text, '/path/to/template')
        assert xml_gconf_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/xml_gconf/'
                  'original.xml', 'r') as original_file:
            original_text = original_file.read()
            xml_gconf_original_object = XMLGConfFormat(original_text,
                                                       '/path/to/template')

        with open('./tests/templates/format/testfiles/xml_gconf/'
                  'template.xml', 'r') as template_file:
            template_text = template_file.read()
            xml_gconf_template_object = XMLGConfFormat(template_text,
                                                       '/path/to/template')

        xml_gconf_original_object.join_template(xml_gconf_template_object)

        with open('./tests/templates/format/testfiles/xml_gconf/'
                  'result.xml', 'r') as result_file:
            result_text = result_file.read()

        assert xml_gconf_original_object.document_text == result_text

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''<?xml version="1.0" encoding="UTF-8"?>
<gconf>
    <dir name="gnome">
        <dir name="ymsgr">
            <entry name="needs_terminal">
                <default type="string">
                    <stringvalue>Something I can never have.</stringvalue>
                </default>
                <local_schema short_desc="Run the command in a terminal">
                    <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
                </local_schema>
            </entry>
            <entry name="logo_icon_name" mtime="1342486180" type="string">
                <stringvalue>distributor</stringvalue>
            </entry>
        </dir>
    </dir>
</gconf>
'''

        template_text = '''<?xml version="1.0" encoding="UTF-8"?>
<gconf>
    <dir name="gnome">
        <dir name="ymsgr">
            <entry name="!logo_icon_name" mtime="1342486180" type="string">
                <stringvalue>distributor</stringvalue>
            </entry>
        </dir>
    </dir>
</gconf>
'''

        join_result = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/template
-->
<gconf>
  <dir name="gnome">
    <dir name="ymsgr">
      <entry name="needs_terminal">
        <default type="string">
          <stringvalue>Something I can never have.</stringvalue>
        </default>
        <local_schema short_desc="Run the command in a terminal">
          <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
        </local_schema>
      </entry>
    </dir>
  </dir>
</gconf>
'''

        original_object = XMLGConfFormat(original_text, '/path/to/template',
                                         add_header=True)
        template_object = XMLGConfFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        print(original_object.document_text)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''<?xml version="1.0" encoding="UTF-8"?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/ancient/template
-->
<gconf>
    <dir name="gnome">
        <dir name="ymsgr">
            <entry name="needs_terminal">
                <default type="string">
                    <stringvalue>Something I can never have.</stringvalue>
                </default>
                <local_schema short_desc="Run the command in a terminal">
                    <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
                </local_schema>
            </entry>
            <entry name="logo_icon_name" mtime="1342486180" type="string">
                <stringvalue>distributor</stringvalue>
            </entry>
        </dir>
    </dir>
</gconf>
'''

        template_text = '''<?xml version="1.0" encoding="UTF-8"?>
<gconf>
    <dir name="gnome">
        <dir name="ymsgr">
            <entry name="!logo_icon_name" mtime="1342486180" type="string">
                <stringvalue>distributor</stringvalue>
            </entry>
        </dir>
    </dir>
</gconf>
'''

        join_result = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/template
-->
<gconf>
  <dir name="gnome">
    <dir name="ymsgr">
      <entry name="needs_terminal">
        <default type="string">
          <stringvalue>Something I can never have.</stringvalue>
        </default>
        <local_schema short_desc="Run the command in a terminal">
          <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
        </local_schema>
      </entry>
    </dir>
  </dir>
</gconf>
'''

        original_object = XMLGConfFormat(original_text, '/path/to/template',
                                         add_header=True)
        template_object = XMLGConfFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        print(original_object.document_text)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''<?xml version="1.0" encoding="UTF-8"?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/ancient/template
-->
<gconf>
    <dir name="gnome">
        <dir name="ymsgr">
            <entry name="needs_terminal">
                <default type="string">
                    <stringvalue>Something I can never have.</stringvalue>
                </default>
                <local_schema short_desc="Run the command in a terminal">
                    <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
                </local_schema>
            </entry>
            <entry name="logo_icon_name" mtime="1342486180" type="string">
                <stringvalue>distributor</stringvalue>
            </entry>
        </dir>
    </dir>
</gconf>
'''

        template_text = '''<?xml version="1.0" encoding="UTF-8"?>
<gconf>
    <dir name="gnome">
        <dir name="ymsgr">
            <entry name="!logo_icon_name" mtime="1342486180" type="string">
                <stringvalue>distributor</stringvalue>
            </entry>
        </dir>
    </dir>
</gconf>
'''

        join_result = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/ancient/template
/path/to/template
-->
<gconf>
  <dir name="gnome">
    <dir name="ymsgr">
      <entry name="needs_terminal">
        <default type="string">
          <stringvalue>Something I can never have.</stringvalue>
        </default>
        <local_schema short_desc="Run the command in a terminal">
          <longdesc>True if the command used to handle this type of URL should be run in a terminal.</longdesc>
        </local_schema>
      </entry>
    </dir>
  </dir>
</gconf>
'''

        original_object = XMLGConfFormat(original_text, '/path/to/template',
                                         add_header=True, already_changed=True)
        template_object = XMLGConfFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        print(original_object.document_text)
        assert original_object.document_text == join_result
