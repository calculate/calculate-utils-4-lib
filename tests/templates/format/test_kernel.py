import pytest
from collections import OrderedDict
from calculate.templates.format.kernel_format import KernelFormat


@pytest.mark.formats
@pytest.mark.kernel
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''
        CONFIG_CC_IS_GCC=y
        CONFIG_GCC_VERSION=90200
        '''

        result = OrderedDict({('', 'CONFIG_CC_IS_GCC'): ['y'],
                              ('', 'CONFIG_GCC_VERSION'): ['90200']})

        kernel_object = KernelFormat(document_text, '/path/to/template')
        assert kernel_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        CONFIG_CLANG_VERSION=0




        CONFIG_CC_HAS_ASM_GOTO=y


        CONFIG_IRQ_WORK=y
        '''

        result = OrderedDict({('', 'CONFIG_CLANG_VERSION'): ['0'],
                              ('', 'CONFIG_CC_HAS_ASM_GOTO'): ['y'],
                              ('', 'CONFIG_IRQ_WORK'): ['y']})

        kernel_object = KernelFormat(document_text, '/path/to/template')
        assert kernel_object._document_dictionary == result

    def test_if_input_document_contains_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''
        -CONFIG_REISERFS_FS=y
        CONFIG_CC_IS_GCC=y
        !CONFIG_IRQ_WORK=y
        '''

        result = OrderedDict({('-', 'CONFIG_REISERFS_FS'): ['y'],
                              ('', 'CONFIG_CC_IS_GCC'): ['y'],
                              ('!', 'CONFIG_IRQ_WORK'): ['y']})

        kernel_object = KernelFormat(document_text, '/path/to/template')
        assert kernel_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''# Comment1
        CONFIG_DEFAULT_HOSTNAME="calculate"

        CONFIG_SWAP=y


        # Comment2
        # Comment3
        CONFIG_SYSVIPC=y
        CONFIG_SYSVIPC_SYSCTL=y

        # Очень важный комментарий, который нужно удалить.
        !CONFIG_CROSS_MEMORY_ATTACH=y
        '''

        result = OrderedDict({('', 'CONFIG_DEFAULT_HOSTNAME'):
                              ['# Comment1', '"calculate"'],
                              ('', 'CONFIG_SWAP'):
                              ['y'],
                              ('', 'CONFIG_SYSVIPC'):
                              ['# Comment2', '# Comment3', 'y'],
                              ('', 'CONFIG_SYSVIPC_SYSCTL'): ['y'],
                              ('!', 'CONFIG_CROSS_MEMORY_ATTACH'):
                              ['# Очень важный комментарий, который нужно удалить.','y']})

        kernel_object = KernelFormat(document_text, '/path/to/template')
        assert kernel_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''
        !CONFIG_REISERFS_FS=y
        !CONFIG_EXT3_FS_POSIX_ACL=
        !CONFIG_EXT3_FS_SECURITY
        '''

        result = OrderedDict({('!', 'CONFIG_REISERFS_FS'): ['y'],
                              ('!', 'CONFIG_EXT3_FS_POSIX_ACL'): [],
                              ('!', 'CONFIG_EXT3_FS_SECURITY'): []})

        kernel_object = KernelFormat(document_text, '/path/to/template')
        assert kernel_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/kernel/original',
                  'r') as original_file:
            original_text = original_file.read()
            kernel_original_object = KernelFormat(original_text,
                                                  '/path/to/template')

        with open('./tests/templates/format/testfiles/kernel/template',
                  'r') as template_file:
            template_text = template_file.read()
            kernel_template_object = KernelFormat(template_text,
                                                  '/path/to/template',
                                                  ignore_comments=True)

        kernel_original_object.join_template(kernel_template_object)

        with open('./tests/templates/format/testfiles/kernel/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert kernel_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''
CONFIG_CC_IS_GCC=y
CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
CONFIG_CC_HAS_ASM_GOTO=y
CONFIG_IRQ_WORK=y
        '''

        document_2 = '''CONFIG_CC_IS_GCC=y
CONFIG_GCC_VERSION=90200
CONFIG_IRQ_WORK=n
CONFIG_BUILDTIME_EXTABLE_SORT=y
CONFIG_THREAD_INFO_IN_TASK=n
CONFIG_EXT3_FS_POSIX_ACL=y
CONFIG_EXT3_FS_SECURITY=n
'''

        document_1_object = KernelFormat(document_1, '/path/to/template')
        document_2_object = KernelFormat(document_2, '/path/to/template')

        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
CONFIG_CC_IS_GCC=y
CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
'''

        template_text = '''
CONFIG_CC_IS_GCC=y
!CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
CONFIG_CC_IS_GCC=y
CONFIG_CLANG_VERSION=0
'''

        original_object = KernelFormat(original_text, '/path/to/template',
                                       add_header=True)
        template_object = KernelFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
CONFIG_CC_IS_GCC=y
CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
'''

        template_text = '''
CONFIG_CC_IS_GCC=y
!CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
CONFIG_CC_IS_GCC=y
CONFIG_CLANG_VERSION=0
'''

        original_object = KernelFormat(original_text, '/path/to/template',
                                       add_header=True)
        template_object = KernelFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
CONFIG_CC_IS_GCC=y
CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
'''

        template_text = '''
CONFIG_CC_IS_GCC=y
!CONFIG_GCC_VERSION=90500
CONFIG_CLANG_VERSION=0
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
CONFIG_CC_IS_GCC=y
CONFIG_CLANG_VERSION=0
'''

        original_object = KernelFormat(original_text, '/path/to/template',
                                       add_header=True, already_changed=True)
        template_object = KernelFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
