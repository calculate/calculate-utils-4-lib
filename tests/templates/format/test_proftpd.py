import pytest
from collections import OrderedDict
from calculate.templates.format.proftpd_format import ProFTPDFormat
from pprint import pprint


@pytest.mark.formats
@pytest.mark.proftpd
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''
        ServerName "ProFTPD Anonymous Server"
        ServerType standalone
        DefaultServer on
        MaxInstances 30
        '''

        result = OrderedDict({('', '', 'ServerName'):
                              ['"ProFTPD Anonymous Server"'],
                              ('', '', 'ServerType'):
                              ['standalone'],
                              ('', '', 'DefaultServer'):
                              ['on'],
                              ('', '', 'MaxInstances'):
                              ['30']})

        proftpd_object = ProFTPDFormat(document_text, '/path/to/template')
        assert proftpd_object._document_dictionary == result

    def test_if_input_document_contains_some_block_of_parameters__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''
        ServerName "ProFTPD Anonymous Server"
        ServerType standalone

        <Anonymous ~ftp>
            <Limit LOGIN>
                DisplayLogin welcome.msg
                DisplayFirstChdir .message
            </Limit>

            User bob
            Group bobs
            UserAlias anonymous ftp

            <Limit WRITE>
                Order allow,deny
                Allow from 10.0.0
                Deny from all
            </Limit>
        </Anonymous>
        '''

        result = OrderedDict({('', '', 'ServerName'):
                              ['"ProFTPD Anonymous Server"'],
                              ('', '', 'ServerType'):
                              ['standalone'],
                              ('', (('Anonymous', '~ftp'), ('Limit', 'LOGIN')),
                               'DisplayLogin'): ['welcome.msg'],
                              ('', (('Anonymous', '~ftp'), ('Limit', 'LOGIN')),
                               'DisplayFirstChdir'): ['.message'],
                              ('', (('Anonymous', '~ftp'), ),
                               'User'): ['bob'],
                              ('', (('Anonymous', '~ftp'), ),
                               'Group'): ['bobs'],
                              ('', (('Anonymous', '~ftp'), ),
                               'UserAlias', 'anonymous', 'ftp'): [''],
                              ('', (('Anonymous', '~ftp'), ('Limit', 'WRITE')),
                               'Order'): ['allow,deny'],
                              ('', (('Anonymous', '~ftp'), ('Limit', 'WRITE')),
                               'Allow from', '10.0.0'): [''],
                              ('', (('Anonymous', '~ftp'), ('Limit', 'WRITE')),
                               'Deny from', 'all'): ['']})

        proftpd_object = ProFTPDFormat(document_text, '/path/to/template')
        assert proftpd_object._document_dictionary == result

    def test_if_input_document_contains_blocks_and_parameters_with_action_marks__the_key_tuples_of_parameters_s_have_it_as_its_first_element_inherited(self):
        document_text = '''
        !ServerName "ProFTPD Anonymous Server"

        <!Anonymous ~ftp>
            <-Limit LOGIN>
                DisplayLogin welcome.msg
                DisplayFirstChdir .message
            </Limit>
        </Anonymous>

        <!Limit WRITE>
            Order allow,deny
            Allow from 10.0.0
            -Deny from all
        </Limit>
        '''

        result = OrderedDict({('!', '', 'ServerName'):
                              ['"ProFTPD Anonymous Server"'],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayLogin'): ['welcome.msg'],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayFirstChdir'): ['.message'],
                              ('!', (('Limit', 'WRITE'), ),
                               'Order'): ['allow,deny'],
                              ('!', (('Limit', 'WRITE'), ),
                               'Allow from', '10.0.0'): [''],
                              ('!', (('Limit', 'WRITE'), ),
                               'Deny from', 'all'): ['']})

        proftpd_object = ProFTPDFormat(document_text, '/path/to/template')
        assert proftpd_object._document_dictionary == result

    def test_if_parameters_and_blocks_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value_or_with_special_key_in_block_dictionary(self):
        document_text = '''
        # Comment 1
        !ServerName "ProFTPD Anonymous Server"

        # Comment 2
        # Comment 3
        <!Anonymous ~ftp>
            <-Limit LOGIN>
                DisplayLogin welcome.msg
                DisplayFirstChdir .message
            </Limit>
        </Anonymous>

        <!Limit WRITE>
            Order allow,deny
            Allow from 10.0.0
            # Comment 4
            -Deny from all
        </Limit>
        '''

        result = OrderedDict({('!', '', 'ServerName'):
                              ['# Comment 1', '"ProFTPD Anonymous Server"'],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayLogin'): ['# Comment 2',
                                                 '# Comment 3',
                                                 'welcome.msg'],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayFirstChdir'): ['.message'],
                              ('!', (('Limit', 'WRITE'), ),
                               'Order'): ['allow,deny'],
                              ('!', (('Limit', 'WRITE'), ),
                               'Allow from', '10.0.0'): [''],
                              ('!', (('Limit', 'WRITE'), ),
                               'Deny from', 'all'): ['# Comment 4', '']})

        proftpd_object = ProFTPDFormat(document_text, '/path/to/template')
        assert proftpd_object._document_dictionary == result

    def test_if_the_ignoreComments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''
        # Comment 1
        ServerName "ProFTPD Anonymous Server"

        # Comment 2
        # Comment 3
        <!Anonymous ~ftp>
            <-Limit LOGIN>
                DisplayLogin welcome.msg
                DisplayFirstChdir .message
            </Limit>
        </Anonymous>

        <Limit WRITE>
            Order allow,deny
            Allow from 10.0.0
            # Comment 4
            !Deny from all
        </Limit>
        '''

        result = OrderedDict({('', '', 'ServerName'):
                              ['"ProFTPD Anonymous Server"'],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayLogin'): ['welcome.msg'],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayFirstChdir'): ['.message'],
                              ('', (('Limit', 'WRITE'), ),
                               'Order'): ['allow,deny'],
                              ('', (('Limit', 'WRITE'), ),
                               'Allow from', '10.0.0'): [''],
                              ('!', (('Limit', 'WRITE'), ),
                               'Deny from', 'all'): ['']})

        proftpd_object = ProFTPDFormat(document_text, '/path/to/template',
                                       ignore_comments=True)
        assert proftpd_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_any_values_the_document_object_contains_dictionary_with_this_items_to_delete(self):
        document_text = '''
        !SQLLog PASS

        <Anonymous ~ftp>
            <Limit LOGIN>
                !DisplayLogin
                DisplayFirstChdir .message
            </Limit>
        </Anonymous>

        <Limit WRITE>
            !Allow from 10.0.0
        </Limit>
        '''

        result = OrderedDict({('!', '', 'SQLLog', 'PASS'): [''],
                              ('!', (('Anonymous', '~ftp'),
                                     ('Limit', 'LOGIN')),
                               'DisplayLogin'): [''],
                              ('', (('Anonymous', '~ftp'),
                                    ('Limit', 'LOGIN')),
                               'DisplayFirstChdir'): ['.message'],
                              ('!', (('Limit', 'WRITE'), ),
                               'Allow from', '10.0.0'): ['']})

        proftpd_object = ProFTPDFormat(document_text, '/path/to/template',
                                       ignore_comments=True)
        assert proftpd_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/proftpd/'
                  'original.conf', 'r') as original_file:
            original_text = original_file.read()
            proftpd_original_object = ProFTPDFormat(original_text,
                                                    '/path/to/template')

        with open('./tests/templates/format/testfiles/proftpd/'
                  'template.conf', 'r') as template_file:
            template_text = template_file.read()
            proftpd_template_object = ProFTPDFormat(template_text,
                                                    '/path/to/template',
                                                    ignore_comments=True)

        proftpd_original_object.join_template(proftpd_template_object)

        with open('./tests/templates/format/testfiles/proftpd/'
                  'result.conf', 'r') as result_file:
            result_text = result_file.read()

        assert proftpd_original_object.document_text == result_text

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
ServerName "ProFTPD Anonymous Server"
ServerType standalone

<Anonymous ~ftp>
    <Limit LOGIN>
        DisplayLogin welcome.msg
        DisplayFirstChdir .message
    </Limit>
    <Limit WRITE>
        Order allow,deny
        Allow from 10.0.0
        Deny from all
    </Limit>
</Anonymous>
'''

        template_text = '''
!ServerType standalone

<Anonymous ~ftp>
    <!Limit WRITE>
        Order allow,deny
        Allow from 10.0.0
        Deny from all
    </Limit>
</Anonymous>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
ServerName "ProFTPD Anonymous Server"
<Anonymous ~ftp>
    <Limit LOGIN>
        DisplayLogin welcome.msg
        DisplayFirstChdir .message
    </Limit>
</Anonymous>
'''

        original_object = ProFTPDFormat(original_text, '/path/to/template',
                                        add_header=True)
        template_object = ProFTPDFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
ServerName "ProFTPD Anonymous Server"
ServerType standalone

<Anonymous ~ftp>
    <Limit LOGIN>
        DisplayLogin welcome.msg
        DisplayFirstChdir .message
    </Limit>
    <Limit WRITE>
        Order allow,deny
        Allow from 10.0.0
        Deny from all
    </Limit>
</Anonymous>
'''

        template_text = '''
!ServerType standalone

<Anonymous ~ftp>
    <!Limit WRITE>
        Order allow,deny
        Allow from 10.0.0
        Deny from all
    </Limit>
</Anonymous>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
ServerName "ProFTPD Anonymous Server"
<Anonymous ~ftp>
    <Limit LOGIN>
        DisplayLogin welcome.msg
        DisplayFirstChdir .message
    </Limit>
</Anonymous>
'''

        original_object = ProFTPDFormat(original_text, '/path/to/template',
                                        add_header=True)
        template_object = ProFTPDFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
ServerName "ProFTPD Anonymous Server"
ServerType standalone

<Anonymous ~ftp>
    <Limit LOGIN>
        DisplayLogin welcome.msg
        DisplayFirstChdir .message
    </Limit>
    <Limit WRITE>
        Order allow,deny
        Allow from 10.0.0
        Deny from all
    </Limit>
</Anonymous>
'''

        template_text = '''
!ServerType standalone

<Anonymous ~ftp>
    <!Limit WRITE>
        Order allow,deny
        Allow from 10.0.0
        Deny from all
    </Limit>
</Anonymous>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
ServerName "ProFTPD Anonymous Server"
<Anonymous ~ftp>
    <Limit LOGIN>
        DisplayLogin welcome.msg
        DisplayFirstChdir .message
    </Limit>
</Anonymous>
'''

        original_object = ProFTPDFormat(original_text, '/path/to/template',
                                        add_header=True, already_changed=True)
        print('ORIGINAL')
        pprint(original_object._document_dictionary)
        template_object = ProFTPDFormat(template_text, '/path/to/template')
        print('TEMPLATE')
        pprint(template_object._document_dictionary)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
