import pytest
from collections import OrderedDict
from calculate.templates.format.postfix_format import PostfixFormat


@pytest.mark.formats
@pytest.mark.postfix
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''parameter_name = /home/divanov/Home
                          other_parameter = yes'''

        result = OrderedDict({('', 'parameter_name'): ['/home/divanov/Home'],
                              ('', 'other_parameter'): ['yes']})

        postfix_object = PostfixFormat(document_text, '/path/to/template')
        assert postfix_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''parameter_name = /home/divanov/Home


                          other_parameter = yes

                          another_parameter = none
                          '''

        result = OrderedDict({('', 'parameter_name'): ['/home/divanov/Home'],
                              ('', 'other_parameter'): ['yes'],
                              ('', 'another_parameter'): ['none']})

        postfix_object = PostfixFormat(document_text, '/path/to/template')
        assert postfix_object._document_dictionary == result

    def test_if_input_document_contains_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''-parameter_name = /home/divanov/Home
                          other_parameter = yes
                          !another_parameter = 1'''

        result = OrderedDict({('-', 'parameter_name'): ['/home/divanov/Home'],
                              ('', 'other_parameter'): ['yes'],
                              ('!', 'another_parameter'): ['1']})

        postfix_object = PostfixFormat(document_text, '/path/to/template')
        assert postfix_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''# Comment1
                          parameter_name = /home/divanov/Home

                          # Comment2
                          # Comment3
                          other_parameter = yes
                          smtp_tls_note_starttls_offer = yes
                          tls_random_source = dev:/dev/urandom

                          # Очень важный комментарий, который нужно удалить.
                          !another_parameter = 1'''

        result = OrderedDict({('', 'parameter_name'):
                              ['# Comment1', '/home/divanov/Home'],
                              ('', 'other_parameter'):
                              ['# Comment2', '# Comment3', 'yes'],
                              ('', 'smtp_tls_note_starttls_offer'): ['yes'],
                              ('', 'tls_random_source'): ['dev:/dev/urandom'],
                              ('!', 'another_parameter'):
                              ['# Очень важный комментарий, который нужно удалить.','1']})

        postfix_object = PostfixFormat(document_text, '/path/to/template')
        assert postfix_object._document_dictionary == result

    def test_if_the_IgnoreComments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''# Comment1
                          parameter_name = /home/divanov/Home

                          # Comment2
                          # Comment3
                          other_parameter = yes
                          smtp_tls_note_starttls_offer = yes
                          tls_random_source = dev:/dev/urandom

                          # Очень важный комментарий, который нужно удалить.
                          !another_parameter = 1'''

        result = OrderedDict({('', 'parameter_name'):
                              ['/home/divanov/Home'],
                              ('', 'other_parameter'):
                              ['yes'],
                              ('', 'smtp_tls_note_starttls_offer'): ['yes'],
                              ('', 'tls_random_source'): ['dev:/dev/urandom'],
                              ('!', 'another_parameter'):
                              ['1']})

        postfix_object = PostfixFormat(document_text, '/path/to/template',
                                       ignore_comments=True)
        assert postfix_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''!parameter_name = /home/divanov/Home
                          !other_parameter =
                          -ignored_line
                          !another_parameter'''

        result = OrderedDict({('!', 'parameter_name'): ['/home/divanov/Home'],
                              ('!', 'other_parameter'): [],
                              ('!', 'another_parameter'): []})

        postfix_object = PostfixFormat(document_text, '/path/to/template')
        assert postfix_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/postfix/original',
                  'r') as original_file:
            original_text = original_file.read()
            postfix_original_object = PostfixFormat(original_text,
                                                    '/path/to/template')

        with open('./tests/templates/format/testfiles/postfix/template',
                  'r') as template_file:
            template_text = template_file.read()
            postfix_template_object = PostfixFormat(template_text,
                                                    '/path/to/template',
                                                    ignore_comments=True)

        postfix_original_object.join_template(postfix_template_object)

        with open('./tests/templates/format/testfiles/postfix/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert postfix_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''
#Рабочая директория Postfix. То место, где временно сохраняется
#вся приходящая почта до процесса доставки.
queue_directory = /var/spool/postfix

#Путь для всех выполняемых программ почтового сервера.
command_directory = /usr/sbin

mynetworks = 10.0.0.0/8, 80.246.243.18, 95.213.228.194, 94.159.1.246, 109.167.151.108, 80.246.245.82, 80.246.248.234, 93.100.239.44

#По умолчанию, Postfix пытается посылать почту в Internet напрямую. В зависимости
#от окружения, в котором функционирует Ваш почтовый сервер, это может быть
#невозможно или нежелательно. Например, Ваша машина может быть отключена от
#Internet-а в нерабочее время, она может быть закрыта файрволлом, Ваш провайдер
#может запрещать пересылку почты в Internet напрямую. В таких случаях Вам
#необходимо настроить Postfix на пересылку писем через другой почтовый сервер
#(relay host).
#Вариант, заключенный в квадратные скобки [], заставляет Postfix
#не предпринимать поиск записей DNS MX.
#по умолчанию: посылать в Internet напрямую
#relayhost = [mail.$mydomain]

relay_domains = lists.calculate-linux.org
#Для создания базы используется postmap
transport_maps = hash:/etc/postfix/transport_maps
relay_recipient_maps = hash:/etc/postfix/valid_recipients
        '''

        document_2 = '''#Рабочая директория Postfix. То место, где временно сохраняется
#вся приходящая почта до процесса доставки.
queue_directory = /var/spool/postfix

#Путь для всех выполняемых программ почтового сервера.
command_directory = /usr/sbin
mynetworks = 10.0.0.0/8, 80.246.243.18, 94.159.1.246, 80.246.245.82, 93.100.239.44
relay_recipient_maps = hash:/etc/postfix/valid_recipient_maps
chmod = 0644
chown = root:root
mail_owner = postfix
'''

        document_1_object = PostfixFormat(document_1, '/path/to/template')
        document_2_object = PostfixFormat(document_2, '/path/to/template')

        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
# Comment1
parameter_name = /home/divanov/Home

# Comment2
# Comment3
other_parameter = yes
smtp_tls_note_starttls_offer = yes
tls_random_source = dev:/dev/urandom
'''

        template_text = '''
!other_parameter
!tls_random_source
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# Comment1
parameter_name = /home/divanov/Home
smtp_tls_note_starttls_offer = yes
'''

        original_object = PostfixFormat(original_text, '/path/to/template',
                                        add_header=True)
        template_object = PostfixFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# Comment1
parameter_name = /home/divanov/Home

# Comment2
# Comment3
other_parameter = yes
smtp_tls_note_starttls_offer = yes
tls_random_source = dev:/dev/urandom
'''

        template_text = '''
!other_parameter
!tls_random_source
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# Comment1
parameter_name = /home/divanov/Home
smtp_tls_note_starttls_offer = yes
'''

        original_object = PostfixFormat(original_text, '/path/to/template',
                                        add_header=True)
        template_object = PostfixFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# Comment1
parameter_name = /home/divanov/Home

# Comment2
# Comment3
other_parameter = yes
smtp_tls_note_starttls_offer = yes
tls_random_source = dev:/dev/urandom
'''

        template_text = '''
!other_parameter
!tls_random_source
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
# Comment1
parameter_name = /home/divanov/Home
smtp_tls_note_starttls_offer = yes
'''

        original_object = PostfixFormat(original_text, '/path/to/template',
                                        add_header=True, already_changed=True)
        template_object = PostfixFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
