import pytest
from collections import OrderedDict
from calculate.templates.format.compiz_format import CompizFormat
from pprint import pprint


@pytest.mark.formats
@pytest.mark.compiz
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''
        [Added Associations]
        application/illustrator=zzz-gimp.desktop
        application/pdf=evince.desktop;
        application/rtf=libreoffice-writer.desktop;
        application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section = OrderedDict({('', 'application/illustrator'):
                               ['zzz-gimp.desktop'],
                               ('', 'application/pdf'):
                               ['evince.desktop;'],
                               ('', 'application/rtf'):
                               ['libreoffice-writer.desktop;'],
                               ('', 'application/vnd.oasis.opendocument.spreadsheet'):
                               ['calculate-calc.desktop;']})

        result = OrderedDict({('', 'Added Associations'): section})

        compiz_object = CompizFormat(document_text, '/path/to/template')
        assert compiz_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        [Added Associations]

        application/illustrator=zzz-gimp.desktop
        application/pdf=evince.desktop;


        application/rtf=libreoffice-writer.desktop;

        application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section = OrderedDict({('', 'application/illustrator'):
                               ['zzz-gimp.desktop'],
                               ('', 'application/pdf'):
                               ['evince.desktop;'],
                               ('', 'application/rtf'):
                               ['libreoffice-writer.desktop;'],
                               ('', 'application/vnd.oasis.opendocument.spreadsheet'):
                               ["calculate-calc.desktop;"]})

        result = OrderedDict({('', 'Added Associations'): section})

        compiz_object = CompizFormat(document_text, '/path/to/template')
        assert compiz_object._document_dictionary == result

    def test_if_input_document_contains_sections_with_different_names_but_different_parameters__the_parameters_merged_in_one_section(self):
        document_text = '''
        [Added Associations]
        application/illustrator=zzz-gimp.desktop
        application/pdf=evince.desktop;
        [Added Associations]
        application/rtf=libreoffice-writer.desktop;
        application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section = OrderedDict({('', 'application/illustrator'):
                               ['zzz-gimp.desktop'],
                               ('', 'application/pdf'):
                               ['evince.desktop;'],
                               ('', 'application/rtf'):
                               ['libreoffice-writer.desktop;'],
                               ('', 'application/vnd.oasis.opendocument.spreadsheet'):
                               ['calculate-calc.desktop;']})

        result = OrderedDict({('', 'Added Associations'): section})

        compiz_object = CompizFormat(document_text, '/path/to/template')
        assert compiz_object._document_dictionary == result

    def test_if_input_document_contains_sections_with_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''
        [Added Associations]
        !application/illustrator=zzz-gimp.desktop
        -application/pdf=evince.desktop;
        !application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section = OrderedDict({('!', 'application/illustrator'):
                               ['zzz-gimp.desktop'],
                               ('-', 'application/pdf'):
                               ['evince.desktop;'],
                               ('!', 'application/vnd.oasis.opendocument.spreadsheet'):
                               ["calculate-calc.desktop;"]})

        result = OrderedDict({('', 'Added Associations'): section})

        compiz_object = CompizFormat(document_text, '/path/to/template')
        assert compiz_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''
        # Comment
        [Added Associations]
        # Comment1
        application/illustrator=zzz-gimp.desktop
        application/pdf=evince.desktop;

        # Comment2
        # Comment3

        application/rtf=libreoffice-writer.desktop;

        [Other Section]
        #Comment
        !application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section_1 = OrderedDict({'#': ['# Comment'],
                                 ('', 'application/illustrator'):
                                 ['# Comment1',
                                  'zzz-gimp.desktop'],
                                 ('', 'application/pdf'):
                                 ['evince.desktop;'],
                                 ('', 'application/rtf'):
                                 ['# Comment2',
                                  '# Comment3',
                                  'libreoffice-writer.desktop;']})

        section_2 = OrderedDict({('!', 'application/vnd.oasis.opendocument.spreadsheet'):
                                 ['#Comment',
                                  "calculate-calc.desktop;"]})

        result = OrderedDict({('', 'Added Associations'): section_1,
                              ('', 'Other Section'): section_2})

        compiz_object = CompizFormat(document_text, '/path/to/template')
        assert compiz_object._document_dictionary == result

    def test_if_the_ignore_comments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''
        # Comment
        [Added Associations]
        # Comment1
        application/illustrator=zzz-gimp.desktop
        application/pdf=evince.desktop;

        # Comment2
        # Comment3

        application/rtf=libreoffice-writer.desktop;

        [Other Section]
        #Comment
        !application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section_1 = OrderedDict({('', 'application/illustrator'):
                                ['zzz-gimp.desktop'],
                                ('', 'application/pdf'):
                                ['evince.desktop;'],
                                ('', 'application/rtf'):
                                ['libreoffice-writer.desktop;']})

        section_2 = OrderedDict({('!', 'application/vnd.oasis.opendocument.spreadsheet'):
                                ["calculate-calc.desktop;"]})

        result = OrderedDict({('', 'Added Associations'): section_1,
                              ('', 'Other Section'): section_2})

        compiz_object = CompizFormat(document_text, '/path/to/template',
                                     ignore_comments=True)
        assert compiz_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values_and_sections_to_delete__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''
        [-Added Associations]
        # Comment
        !application/illustrator=zzz-gimp.desktop
        !application/pdf=
        !application/rtf

        [!Other Section]
        application/vnd.oasis.opendocument.spreadsheet=calculate-calc.desktop;
        '''

        section_1 = OrderedDict({('!', 'application/illustrator'):
                                ['# Comment',
                                 'zzz-gimp.desktop'],
                                ('!', 'application/pdf'): [],
                                ('!', 'application/rtf'): []})

        section_2 = OrderedDict({('', 'application/vnd.oasis.opendocument.spreadsheet'):
                                ["calculate-calc.desktop;"]})

        result = OrderedDict({('-', 'Added Associations'): section_1,
                              ('!', 'Other Section'): section_2})

        compiz_object = CompizFormat(document_text, '/path/to/template')
        assert compiz_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/compiz/original',
                  'r') as original_file:
            original_text = original_file.read()
            compiz_original_object = CompizFormat(original_text,
                                                  '/path/to/template')

        with open('./tests/templates/format/testfiles/compiz/template',
                  'r') as template_file:
            template_text = template_file.read()
            compiz_template_object = CompizFormat(template_text,
                                                  '/path/to/template',
                                                  ignore_comments=True)

        compiz_original_object.join_template(compiz_template_object)

        with open('./tests/templates/format/testfiles/compiz/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert compiz_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''[Added Associations]
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;
application/rtf=libreoffice-writer.desktop;

[Non Realistic Section]
audio/mp4=clementine.desktop;
audio/mpeg=clementine.desktop;
audio/x-flac=clementine.desktop;
        '''

        document_2 = '''[Added Associations]
image/bmp=gwenview.desktop;
image/gif=gwenview.desktop;
image/jpeg=gwenview.desktop;
image/jpg=gwenview.desktop;
image/png=gwenview.desktop;

[Non Realistic Section]
audio/mp4=clementine.desktop;
audio/mpeg=clementine.desktop;
audio/x-ms-wma=clementine.desktop;
audio/x-wav=clementine.desktop;

[Strange Section]
video/mp4=smplayer.desktop;
video/mpeg=smplayer.desktop;
video/quicktime=smplayer.desktop;
video/vnd.mpegurl=smplayer.desktop;
'''

        document_1_object = CompizFormat(document_1, '/path/to/template')
        document_2_object = CompizFormat(document_2, '/path/to/template')
        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;

# Comment2
# Comment3

application/rtf=libreoffice-writer.desktop;

[Strange Section]
video/mp4=smplayer.desktop;
video/mpeg=smplayer.desktop;
'''

        template_text = '''
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;

# Comment2
# Comment3

!application/rtf

[!Strange Section]
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;
'''

        original_object = CompizFormat(original_text, '/path/to/template',
                                       add_header=True)
        template_object = CompizFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;

# Comment2
# Comment3

application/rtf=libreoffice-writer.desktop;

[Strange Section]
video/mp4=smplayer.desktop;
video/mpeg=smplayer.desktop;
'''

        template_text = '''
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;

# Comment2
# Comment3

!application/rtf

[!Strange Section]
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;
'''

        original_object = CompizFormat(original_text, '/path/to/template',
                                       add_header=True)
        template_object = CompizFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;

# Comment2
# Comment3

application/rtf=libreoffice-writer.desktop;

[Strange Section]
video/mp4=smplayer.desktop;
video/mpeg=smplayer.desktop;
'''

        template_text = '''
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;

# Comment2
# Comment3

!application/rtf

[!Strange Section]
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
# Comment
[Added Associations]
# Comment1
application/illustrator=zzz-gimp.desktop
application/pdf=evince.desktop;
'''

        original_object = CompizFormat(original_text, '/path/to/template',
                                       add_header=True, already_changed=True)
        template_object = CompizFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
