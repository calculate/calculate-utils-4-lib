import pytest
from collections import OrderedDict
from calculate.templates.format.kde_format import KDEFormat


@pytest.mark.formats
@pytest.mark.kde
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''[section name][first][second]
            parameter name=/home/divanov/Home
            other parameter=yes'''

        param_line_1 = OrderedDict({('', 'parameter name'):
                                    ['/home/divanov/Home']})
        param_line_2 = OrderedDict({('', 'other parameter'): ['yes']})

        result = OrderedDict({('', 'section name', 'first', 'second'):
                              OrderedDict(**param_line_1,
                                          **param_line_2)})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_if_input_document_contains_parameters_with_values_with_unicode_symbols__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        [Desktop Entry][val]
        GenericName=IRC Client
        GenericName[ar]=عميل IRC
        GenericName[be]=Кліент IRC
        GenericName[bg]=IRC клиент
        GenericName[bs]=IRC klijent
        GenericName[ca]=Client d'IRC
        GenericName[ca@valencia]=Client d'IRC'''

        section = OrderedDict({('', 'GenericName'): ['IRC Client'],
                               ('', 'GenericName[ar]'): ['عميل IRC'],
                               ('', 'GenericName[be]'): ['Кліент IRC'],
                               ('', 'GenericName[bg]'): ['IRC клиент'],
                               ('', 'GenericName[bs]'): ['IRC klijent'],
                               ('', 'GenericName[ca]'): ["Client d'IRC"],
                               ('', 'GenericName[ca@valencia]'):
                               ["Client d'IRC"]})

        result = OrderedDict({('', 'Desktop Entry', 'val'): section})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        [PlasmaViews][Panel 69][Horizontal2048]
            alignment=132

            length=674


            thickness = 56

        [Desktop Entry]

            Exec=konversation -qwindowtitle %c %u
            '''

        section_1_content = OrderedDict({('', 'alignment'): ['132'],
                                         ('', 'length'): ['674'],
                                         ('', 'thickness'): [' 56']})

        section_2_content = OrderedDict({('', 'Exec'):
                                         ['konversation -qwindowtitle %c %u']})

        result = OrderedDict({('', 'PlasmaViews', 'Panel 69',
                               'Horizontal2048'): section_1_content,
                              ('', 'Desktop Entry'): section_2_content})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_if_input_document_contains_sections_with_different_names_but_different_parameters__the_parameters_merged_in_one_section(self):
        document_text = '''
        [PlasmaViews][Panel 69][Horizontal2048]
            alignment=132

            length=674

        [PlasmaViews][Panel 69][Horizontal2048]

            thickness = 56

                            '''

        section_1_content = OrderedDict({('', 'alignment'): ['132'],
                                         ('', 'length'): ['674'],
                                         ('', 'thickness'): [' 56']})

        result = OrderedDict({('', 'PlasmaViews', 'Panel 69',
                               'Horizontal2048'): section_1_content})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_if_input_document_contains_sections_with_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''
        [PlasmaViews][Panel 69][Horizontal2048]
            !alignment=132
            length=674
            -thickness = 56
            '''

        section_1_content = OrderedDict({('!', 'alignment'): ['132'],
                                         ('', 'length'): ['674'],
                                         ('-', 'thickness'): [' 56']})

        result = OrderedDict({('', 'PlasmaViews', 'Panel 69',
                               'Horizontal2048'): section_1_content})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''
        # Comment to section
        [PlasmaViews][Panel 69][Horizontal2048]
            # Comment 1
            alignment=132
            length=674

            # Comment 2
            #Comment 3
            thickness = 56

        [Desktop Entry]
            # Comment

            Exec=konversation -qwindowtitle %c %u
            '''

        section_1_content = OrderedDict({'#': ['# Comment to section'],
                                         ('', 'alignment'): ['# Comment 1',
                                                             '132'],
                                         ('', 'length'): ['674'],
                                         ('', 'thickness'): ['# Comment 2',
                                                             '#Comment 3',
                                                             ' 56']})

        section_2_content = OrderedDict({('', 'Exec'):
                                         ['# Comment',
                                          'konversation -qwindowtitle %c %u']})

        result = OrderedDict({('', 'PlasmaViews', 'Panel 69',
                               'Horizontal2048'): section_1_content,
                              ('', 'Desktop Entry'): section_2_content})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_if_the_IgnoreComments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''
        # Comment to section
        [PlasmaViews][Panel 69][Horizontal2048]
            # Comment 1
            alignment=132
            length=674

            # Comment 2
            #Comment 3
            thickness = 56

        [Desktop Entry]
            # Comment

            Exec=konversation -qwindowtitle %c %u
            '''

        section_1_content = OrderedDict({('', 'alignment'): ['132'],
                                         ('', 'length'): ['674'],
                                         ('', 'thickness'): [' 56']})

        section_2_content = OrderedDict({('', 'Exec'):
                                         ['konversation -qwindowtitle %c %u']})

        result = OrderedDict({('', 'PlasmaViews', 'Panel 69',
                               'Horizontal2048'): section_1_content,
                              ('', 'Desktop Entry'): section_2_content})

        kde_object = KDEFormat(document_text, '/path/to/template',
                               ignore_comments=True)
        assert kde_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values_and_sections_to_delete__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''
        [PlasmaViews][Panel 69][Horizontal2048]
            alignment=132
            !length =
            !thickness
        [!PlasmaViews][Panel 69]
            alignment=132
            panelVisibility=1
        [-Desktop Entry]
            Exec=konversation -qwindowtitle %c %u
            '''

        section_1_content = OrderedDict({('', 'alignment'): ['132'],
                                         ('!', 'length'): [],
                                         ('!', 'thickness'): []})

        section_2_content = OrderedDict({('', 'alignment'): ['132'],
                                         ('', 'panelVisibility'): ['1']})

        section_3_content = OrderedDict({('', 'Exec'):
                                         ['konversation -qwindowtitle %c %u']})

        result = OrderedDict({('', 'PlasmaViews', 'Panel 69',
                               'Horizontal2048'):
                              section_1_content,
                              ('!', 'PlasmaViews', 'Panel 69'):
                              section_2_content,
                              ('-', 'Desktop Entry'):
                              section_3_content})

        kde_object = KDEFormat(document_text, '/path/to/template')
        assert kde_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/kde/original',
                  'r') as original_file:
            original_text = original_file.read()
            kde_original_object = KDEFormat(original_text, '/path/to/template')

        with open('./tests/templates/format/testfiles/kde/template',
                  'r') as template_file:
            template_text = template_file.read()
            kde_template_object = KDEFormat(template_text, '/path/to/template',
                                            ignore_comments=True)

        kde_original_object.join_template(kde_template_object)

        with open('./tests/templates/format/testfiles/kde/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert kde_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''[PlasmaViews][Panel 69][Horizontal 1024]
alignment=132
length=674
panelVisibility=1
thickness=56

[PlasmaViews][Panel 69]
alignment=124
parameter=true

[PlasmaRunnerManager]
LaunchCounts=None
pluginWhiteList=services,shell,bookmarks,baloosearch,locations'''

        document_2 = '''[PlasmaViews][Panel 69][Horizontal 1024]
alignment=128
panelVisibility=1
maxLength=674
minLength=674

[PlasmaRunnerManager]
Count=What the freakin count...
pluginWhiteList=shell,bookmarks,locations

[FileDialogSize]
Height 1080=466
Width 1920=747
'''

        document_1_object = KDEFormat(document_1, '/path/to/template')
        document_2_object = KDEFormat(document_2, '/path/to/template')

        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
[PlasmaRunnerManager]
Count=What the freakin count...
pluginWhiteList=shell,bookmarks,locations

[FileDialogSize]
Height 1080=466
Width 1920=747
'''

        template_text = '''
[PlasmaRunnerManager]
!Count=What the freakin count...

[!FileDialogSize]
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
[PlasmaRunnerManager]
pluginWhiteList=shell,bookmarks,locations
'''

        original_object = KDEFormat(original_text, '/path/to/template',
                                    add_header=True)
        template_object = KDEFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
[PlasmaRunnerManager]
Count=What the freakin count...
pluginWhiteList=shell,bookmarks,locations

[FileDialogSize]
Height 1080=466
Width 1920=747
'''

        template_text = '''
[PlasmaRunnerManager]
!Count=What the freakin count...

[!FileDialogSize]
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
[PlasmaRunnerManager]
pluginWhiteList=shell,bookmarks,locations
'''

        original_object = KDEFormat(original_text, '/path/to/template',
                                    add_header=True)
        template_object = KDEFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
[PlasmaRunnerManager]
Count=What the freakin count...
pluginWhiteList=shell,bookmarks,locations

[FileDialogSize]
Height 1080=466
Width 1920=747
'''

        template_text = '''
[PlasmaRunnerManager]
!Count=What the freakin count...

[!FileDialogSize]
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
[PlasmaRunnerManager]
pluginWhiteList=shell,bookmarks,locations
'''

        original_object = KDEFormat(original_text, '/path/to/template',
                                    add_header=True, already_changed=True)
        template_object = KDEFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_with_xa0_symbol_in_source_document(self):
        original_text = '''[Test]
Name[pl]=Przechowalnia certyfikatów i\xa0kluczy
Comment[fr]=Trousseau de clés de GNOME\xa0: composant PKCS#11
NoShow=true
'''

        template_text = '''[Test]
!NoShow=
'''

        join_result = '''[Test]
Name[pl]=Przechowalnia certyfikatów i\xa0kluczy
Comment[fr]=Trousseau de clés de GNOME\xa0: composant PKCS#11
'''

        original_object = KDEFormat(original_text, '/path/to/template')
        print("ORIGINAL DICT:")
        original_object.print_dictionary()
        template_object = KDEFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_with_space_at_end_of_parameter(self):
        original_text = '''[Test]
Name[pl]=Hello 
NoShow=true
'''

        template_text = '''[Test]
!NoShow=
'''

        join_result = '''[Test]
Name[pl]=Hello 
'''

        original_object = KDEFormat(original_text, '/path/to/template')
        template_object = KDEFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_with_space_at_the_begin_and_the_end_of_parameter_value(self):
        original_text = '''[Test]
Name[pl]=  Hello 
NoShow=true
'''

        template_text = '''[Test]
!NoShow=
'''

        join_result = '''[Test]
Name[pl]=  Hello 
'''

        original_object = KDEFormat(original_text, '/path/to/template')
        template_object = KDEFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_parsing_text(self):
        with open('./tests/templates/format/testfiles/kde/example',
                  'r') as example_file:
            example_text = example_file.read()
            KDEFormat(example_text, '/path/to/template')
