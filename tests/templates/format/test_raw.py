import pytest
from calculate.templates.format.raw_format import RawFormat
from calculate.templates.template_engine import ParametersContainer


@pytest.mark.formats
@pytest.mark.raw
class TestParsingMethods:
    def test_first(self):
        original = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;'''

        template = '''parameter-4 = d;
parameter-5 = e;'''

        output = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;
parameter-4 = d;
parameter-5 = e;
'''

        raw_document_1 = RawFormat(original, 'path/to/template')
        raw_document_2 = RawFormat(template, 'path/to/template')
        raw_document_1.join_template(raw_document_2)

        assert raw_document_1.document_text == output

    def test_second(self):
        document_1 = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;'''

        document_2 = '''parameter-4 = d;
parameter-5 = e;'''

        output = '''parameter-4 = d;
parameter-5 = e;
parameter-1 = a;
parameter-2 = b;
parameter-3 = c;
'''

        raw_document_1 = RawFormat(document_1, 'path/to/template',
                                   join_before=True)
        raw_document_2 = RawFormat(document_2, 'path/to/template')
        raw_document_1.join_template(raw_document_2)

        assert raw_document_1.document_text == output

    def test_first_with_comment(self):
        parameters = ParametersContainer({'comment': '#'})
        document_1 = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;'''

        document_2 = '''parameter-4 = d;
parameter-5 = e;'''

        output = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# path/to/template
#-------------------------------------------------------------------------------
parameter-1 = a;
parameter-2 = b;
parameter-3 = c;
parameter-4 = d;
parameter-5 = e;
'''

        raw_document_1 = RawFormat(document_1, 'path/to/template',
                                   add_header=True, parameters=parameters)
        raw_document_2 = RawFormat(document_2, 'path/to/template')
        raw_document_1.join_template(raw_document_2)

        assert raw_document_1.document_text == output

    def test_second_with_comment(self):
        parameters = ParametersContainer({'comment': 'xml'})
        document_1 = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;'''

        document_2 = '''parameter-4 = d;
parameter-5 = e;'''

        output = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
path/to/template
-->
parameter-4 = d;
parameter-5 = e;
parameter-1 = a;
parameter-2 = b;
parameter-3 = c;
'''

        raw_document_1 = RawFormat(document_1, 'path/to/template',
                                   join_before=True, add_header=True,
                                   parameters=parameters)
        raw_document_2 = RawFormat(document_2, 'path/to/template')
        raw_document_1.join_template(raw_document_2)

        assert raw_document_1.document_text == output

    def test_third_with_comment(self):
        parameters = ParametersContainer({'comment': '#'})
        original_text = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;'''

        template_text_1 = '''parameter-4 = d;
parameter-5 = e;'''

        template_text_2 = '''parameter-6 = f;'''

        output = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# path/to/template_1
# path/to/template_2
#-------------------------------------------------------------------------------
parameter-1 = a;
parameter-2 = b;
parameter-3 = c;
parameter-4 = d;
parameter-5 = e;
parameter-6 = f;
'''

        original_1 = RawFormat(original_text, 'path/to/template_1',
                               add_header=True,
                               already_changed=False,
                               parameters=parameters)
        template_1 = RawFormat(template_text_1, 'path/to/template_1')
        template_2 = RawFormat(template_text_2, 'path/to/template_2')

        original_1.join_template(template_1)
        original_2 = RawFormat(original_1.document_text, 'path/to/template_2',
                               add_header=True,
                               already_changed=True,
                               parameters=parameters)

        original_2.join_template(template_2)

        assert original_2.document_text == output

    def test_fourth_with_empty_comment_symbol(self):
        parameters = ParametersContainer({'comment': ''})
        original_text = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;'''

        template_text_1 = '''parameter-4 = d;
parameter-5 = e;'''

        template_text_2 = '''parameter-6 = f;'''

        output = '''parameter-1 = a;
parameter-2 = b;
parameter-3 = c;
parameter-4 = d;
parameter-5 = e;
parameter-6 = f;
'''

        original_1 = RawFormat(original_text, 'path/to/template_1',
                               add_header=True,
                               already_changed=False,
                               parameters=parameters)
        template_1 = RawFormat(template_text_1, 'path/to/template_1')
        template_2 = RawFormat(template_text_2, 'path/to/template_2')

        original_1.join_template(template_1)
        original_2 = RawFormat(original_1.document_text, 'path/to/template_2',
                               add_header=True,
                               already_changed=True)

        original_2.join_template(template_2)

        assert original_2.document_text == output

    def test_shebang(self):
        parameters = ParametersContainer({'comment': '#'})
        original_text = '''#!/bin/bash
echo "message_1"'''

        template_text_1 = '''echo "message_2"
exit 0'''

        output = '''#!/bin/bash
#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# path/to/template_1
#-------------------------------------------------------------------------------
echo "message_1"
echo "message_2"
exit 0
'''

        original_1 = RawFormat(original_text, 'path/to/template_1',
                               add_header=True,
                               already_changed=False,
                               parameters=parameters)
        template_1 = RawFormat(template_text_1, 'path/to/template_1')
        original_1.join_template(template_1)
        assert original_1.document_text == output

    def test_shebang_and_comment(self):
        parameters = ParametersContainer({'comment': '#'})
        original_text = '''#!/bin/bash
#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# path/to/ancient/template
#-------------------------------------------------------------------------------
echo "message_1"'''

        template_text_1 = '''#!/bin/babash
echo "message_2"
exit 0'''

        output = '''#!/bin/babash
#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# path/to/ancient/template
# path/to/template_1
#-------------------------------------------------------------------------------
echo "message_1"
echo "message_2"
exit 0
'''

        original_1 = RawFormat(original_text, 'path/to/template_1',
                               add_header=True,
                               already_changed=True,
                               parameters=parameters)
        template_1 = RawFormat(template_text_1, 'path/to/template_1')
        original_1.join_template(template_1)
        assert original_1.document_text == output

    def test_make_shebang_great_again(self):
        parameters = ParametersContainer({'comment': '#'})
        original_text = ''

        template_text_1 = '''#!/bin/bash
echo "message"
exit 0'''

        output = '''#!/bin/bash
#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# path/to/template_1
#-------------------------------------------------------------------------------
echo "message"
exit 0
'''

        original_1 = RawFormat(original_text, 'path/to/template_1',
                               add_header=True,
                               already_changed=False,
                               parameters=parameters)
        template_1 = RawFormat(template_text_1, 'path/to/template_1')
        original_1.join_template(template_1)
        assert original_1.document_text == output
