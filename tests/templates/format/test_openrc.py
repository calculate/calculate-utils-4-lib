import pytest
from collections import OrderedDict
from calculate.templates.format.openrc_format import OpenRCFormat


@pytest.mark.formats
@pytest.mark.openrc
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''
        rc_interactive="NO"
        INSTANCE="openldap${SVCNAME#slapd}"
        OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"
        OPTS="${OPTS_CONF} -h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'"
        '''

        result = OrderedDict({('', 'rc_interactive'): ['"NO"'],
                              ('', 'INSTANCE'): ['"openldap${SVCNAME#slapd}"'],
                              ('', 'OPTS_CONF'): ['"-f /etc/${INSTANCE}/slapd.conf"'],
                              ('', 'OPTS'): ['"${OPTS_CONF} -h \'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock\'"']})

        openrc_object = OpenRCFormat(document_text, '/path/to/template')
        assert openrc_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        rc_interactive="NO"


        INSTANCE="openldap${SVCNAME#slapd}"

        OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"


        OPTS="${OPTS_CONF} -h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'"
        '''

        result = OrderedDict({('', 'rc_interactive'): ['"NO"'],
                              ('', 'INSTANCE'): ['"openldap${SVCNAME#slapd}"'],
                              ('', 'OPTS_CONF'): ['"-f /etc/${INSTANCE}/slapd.conf"'],
                              ('', 'OPTS'): ['"${OPTS_CONF} -h \'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock\'"']})

        openrc_object = OpenRCFormat(document_text, '/path/to/template')
        assert openrc_object._document_dictionary == result

    def test_if_input_document_contains_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''
        !rc_interactive="NO"
        -INSTANCE="openldap${SVCNAME#slapd}"
        OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"
        !OPTS="${OPTS_CONF} -h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'"
        '''

        result = OrderedDict({('!', 'rc_interactive'): ['"NO"'],
                              ('-', 'INSTANCE'): ['"openldap${SVCNAME#slapd}"'],
                              ('', 'OPTS_CONF'): ['"-f /etc/${INSTANCE}/slapd.conf"'],
                              ('!', 'OPTS'): ['"${OPTS_CONF} -h \'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock\'"']})

        openrc_object = OpenRCFormat(document_text, '/path/to/template')
        assert openrc_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''
        # If you have multiple slapd instances per #376699, this will provide a default config
        rc_interactive="NO"
        INSTANCE="openldap${SVCNAME#slapd}"

        # Comment1
        # Comment2

        OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"


        OPTS="${OPTS_CONF} -h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'"
        '''

        result = OrderedDict({('', 'rc_interactive'):
                              ['# If you have multiple slapd instances per #376699, this will provide a default config', '"NO"'],
                              ('', 'INSTANCE'):
                              ['"openldap${SVCNAME#slapd}"'],
                              ('', 'OPTS_CONF'):
                              ['# Comment1',
                               '# Comment2',
                               '"-f /etc/${INSTANCE}/slapd.conf"'],
                              ('', 'OPTS'):
                              ['"${OPTS_CONF} -h \'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock\'"']})

        openrc_object = OpenRCFormat(document_text, '/path/to/template')
        assert openrc_object._document_dictionary == result

    def test_if_the_IgnoreComments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''
        # If you have multiple slapd instances per #376699, this will provide a default config
        rc_interactive="NO"
        INSTANCE="openldap${SVCNAME#slapd}"

        # Comment1
        # Comment2

        OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"


        OPTS="${OPTS_CONF} -h 'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock'"
        '''

        result = OrderedDict({('', 'rc_interactive'):
                              ['"NO"'],
                              ('', 'INSTANCE'):
                              ['"openldap${SVCNAME#slapd}"'],
                              ('', 'OPTS_CONF'):
                              ['"-f /etc/${INSTANCE}/slapd.conf"'],
                              ('', 'OPTS'):
                              ['"${OPTS_CONF} -h \'ldaps:// ldap:// ldapi://%2fvar%2frun%2fopenldap%2fslapd.sock\'"']})

        openrc_object = OpenRCFormat(document_text, '/path/to/template',
                                     ignore_comments=True)
        assert openrc_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''
        !rc_interactive="NO"
        !INSTANCE=
        !OPTS_CONF
        '''

        result = OrderedDict({('!', 'rc_interactive'): ['"NO"'],
                              ('!', 'INSTANCE'): [],
                              ('!', 'OPTS_CONF'): []})

        openrc_object = OpenRCFormat(document_text,
                                     '/path/to/template')
        assert openrc_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/openrc/original',
                  'r') as original_file:
            original_text = original_file.read()
            openrc_original_object = OpenRCFormat(original_text,
                                                  '/path/to/template')

        with open('./tests/templates/format/testfiles/openrc/template',
                  'r') as template_file:
            template_text = template_file.read()
            openrc_template_object = OpenRCFormat(template_text,
                                                  '/path/to/template',
                                                  ignore_comments=True)

        openrc_original_object.join_template(openrc_template_object)

        with open('./tests/templates/format/testfiles/openrc/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert openrc_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''rc_tty_number=12

# The following setting turns on the memory.use_hierarchy setting in the
# root memory cgroup for cgroups v1.
# It must be set to yes in this file if you want this functionality.
rc_cgroup_memory_use_hierarchy="NO"

# This sets the mode used to mount cgroups.
# "hybrid" mounts cgroups version 2 on /sys/fs/cgroup/unified and
# cgroups version 1 on /sys/fs/cgroup.
# "legacy" mounts cgroups version 1 on /sys/fs/cgroup
# "unified" mounts cgroups version 2 on /sys/fs/cgroup
rc_cgroup_mode="hybrid"

# If you use the classical configuration file:
opts_conf="-f /etc/${INSTANCE}/slapd.conf"

# Multiple settings and values can be specified.
# For example, you would use this to set the maximum memory and maximum
# number of pids for a service.
rc_cgroup_settings="10485760"'''

        document_2 = '''rc_tty_number=12

# The following setting turns on the memory.use_hierarchy setting in the
# root memory cgroup for cgroups v1.
# It must be set to yes in this file if you want this functionality.
rc_cgroup_memory_use_hierarchy="YES"

# Multiple settings and values can be specified.
# For example, you would use this to set the maximum memory and maximum
# number of pids for a service.
rc_cgroup_settings="10485760"
instance="openldap${SVCNAME#slapd}"
rc_cgroup_cleanup="NO"
rc_send_sighup="YES"
'''

        document_1_object = OpenRCFormat(document_1, '/path/to/template')
        document_2_object = OpenRCFormat(document_2, '/path/to/template')

        template = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''
# If you have multiple slapd instances per #376699, this will provide a default config
rc_interactive="NO"
INSTANCE="openldap${SVCNAME#slapd}"
# Comment1
# Comment2
OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"
'''

        template_text = '''
!OPTS_CONF
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# If you have multiple slapd instances per #376699, this will provide a default config
rc_interactive="NO"
INSTANCE="openldap${SVCNAME#slapd}"
'''

        original_object = OpenRCFormat(original_text, '/path/to/template',
                                       add_header=True)
        template_object = OpenRCFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# If you have multiple slapd instances per #376699, this will provide a default config
rc_interactive="NO"
INSTANCE="openldap${SVCNAME#slapd}"
# Comment1
# Comment2
OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"
'''

        template_text = '''
RC_INTERACTIVE="NO"
!OPTS_CONF
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# If you have multiple slapd instances per #376699, this will provide a default config
RC_INTERACTIVE="NO"
INSTANCE="openldap${SVCNAME#slapd}"
'''

        original_object = OpenRCFormat(original_text, '/path/to/template',
                                       add_header=True)
        template_object = OpenRCFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# If you have multiple slapd instances per #376699, this will provide a default config
rc_interactive="NO"
INSTANCE="openldap${SVCNAME#slapd}"

# Comment1
# Comment2
OPTS_CONF="-f /etc/${INSTANCE}/slapd.conf"
'''

        template_text = '''
!OPTS_CONF
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
# If you have multiple slapd instances per #376699, this will provide a default config
rc_interactive="NO"
INSTANCE="openldap${SVCNAME#slapd}"
'''

        original_object = OpenRCFormat(original_text, '/path/to/template',
                                       add_header=True, already_changed=True)
        template_object = OpenRCFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_register_changing(self):
        original = '''PARAM=1
param2=2
BIG_PARAM=1
low_param=2'''

        template = '''big_param=3
LOW_PARAM=4
new_param=5
NEW_PARAM2=6'''

        result = '''PARAM=1
param2=2
big_param=3
LOW_PARAM=4
new_param=5
NEW_PARAM2=6
'''

        original_object = OpenRCFormat(original, '/path/to/template')
        template_object = OpenRCFormat(template, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == result
