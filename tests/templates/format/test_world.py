import pytest
from collections import OrderedDict
from calculate.templates.format.world_format import WorldFormat
import os


CHROOT_PATH = os.path.join(os.getcwd(),
                           "tests/templates/format/testfiles/world")


@pytest.mark.formats
@pytest.mark.world
def test_parsing_document():
    original_text = """www-client/chromium:unstable
dev-python/pip
dev-util/cmake
clementine
"""

    original_object = WorldFormat(original_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    result_dict = OrderedDict({"www-client":
                               OrderedDict({"chromium":
                                           OrderedDict({"unstable": None})}),
                               "dev-python":
                               OrderedDict({"pip":
                                           OrderedDict({None: None})}),
                               "dev-util":
                               OrderedDict({"cmake":
                                           OrderedDict({None: None})}),
                               "media-sound":
                               OrderedDict({"clementine":
                                           OrderedDict({None: None})})})
    assert original_object._document_dictionary == result_dict


@pytest.mark.formats
@pytest.mark.world
def test_parsing_template():
    template_text = """www-client/chromium:stable
!!www-client/chromium:unstable
!www-client/chromium
dev-util/cmake
alsa-utils
"""

    template_object = WorldFormat(template_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    result_dict = OrderedDict({"www-client":
                               OrderedDict({"chromium":
                                           OrderedDict({"stable": None,
                                                        "unstable": '!',
                                                        None: '!'})}),
                               "dev-util":
                               OrderedDict({"cmake":
                                           OrderedDict({None: None})}),
                               "media-sound":
                               OrderedDict({"alsa-utils":
                                           OrderedDict({None: None})})})
    assert template_object._document_dictionary == result_dict


@pytest.mark.formats
@pytest.mark.world
def test_parsing_template_with_warning():
    template_text = """www-client/chromium:stable
!!www-client/chromium:not_exist
!www-client/chromium
dev-util/cmake
alsa-utils
"""

    template_object = WorldFormat(template_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    result_dict = OrderedDict({"www-client":
                               OrderedDict({"chromium":
                                           OrderedDict({"stable": None,
                                                        "not_exist": '!',
                                                        None: '!'})}),
                               "dev-util":
                               OrderedDict({"cmake":
                                           OrderedDict({None: None})}),
                               "media-sound":
                               OrderedDict({"alsa-utils":
                                           OrderedDict({None: None})})})
    assert template_object.warnings == [
                           "package 'www-client/chromium:not_exist' not found"]
    assert template_object._document_dictionary == result_dict


@pytest.mark.formats
@pytest.mark.world
def test_joining_template():
    original_text = """www-client/chromium:unstable
dev-python/pip
dev-util/cmake
clementine
"""
    template_text = """www-client/chromium:stable
!!www-client/chromium:unstable
!www-client/chromium
dev-util/cmake
alsa-utils
"""

    original_object = WorldFormat(original_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    template_object = WorldFormat(template_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    result_dict = OrderedDict({"www-client":
                               OrderedDict({"chromium":
                                           OrderedDict({"stable": None})}),
                               "dev-python":
                               OrderedDict({"pip":
                                           OrderedDict({None: None})}),
                               "dev-util":
                               OrderedDict({"cmake":
                                           OrderedDict({None: None})}),
                               "media-sound":
                               OrderedDict({"clementine":
                                            OrderedDict({None: None}),
                                            "alsa-utils":
                                            OrderedDict({None: None})})})

    original_object.join_template(template_object)
    assert original_object._document_dictionary == result_dict


@pytest.mark.formats
@pytest.mark.world
def test_joining_and_get_text_without_header():
    original_text = """www-client/chromium:unstable
dev-python/pip
dev-util/cmake
clementine
"""
    template_text = """www-client/chromium:stable
!!www-client/chromium:unstable
!www-client/chromium
dev-util/cmake
alsa-utils
"""
    result_text = """dev-python/pip
dev-util/cmake
media-sound/alsa-utils
media-sound/clementine
www-client/chromium:stable
"""

    original_object = WorldFormat(original_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    template_object = WorldFormat(template_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    original_object.join_template(template_object)
    assert original_object.document_text == result_text


@pytest.mark.formats
@pytest.mark.world
def test_joining_and_get_text_with_header():
    original_text = """www-client/chromium:unstable
dev-python/pip
dev-util/cmake
clementine
"""
    template_text = """www-client/chromium:stable
!!www-client/chromium:unstable
!www-client/chromium
dev-util/cmake
alsa-utils
"""
    result_text = """#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template/media-sound
#-------------------------------------------------------------------------------
dev-python/pip
dev-util/cmake
media-sound/alsa-utils
media-sound/clementine
www-client/chromium:stable
"""

    original_object = WorldFormat(original_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH,
                                  add_header=True)
    template_object = WorldFormat(template_text,
                                  "/path/to/template/media-sound",
                                  chroot_path=CHROOT_PATH)
    original_object.join_template(template_object)
    assert original_object.document_text == result_text
