import pytest
from collections import OrderedDict
from calculate.templates.format.xml_xfce_format import XMLXfceFormat


@pytest.mark.formats
@pytest.mark.xml_xfce
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''<?xml version="1.0" encoding="UTF-8"?>
            <channel name="xsettings" version="1.0">
              <property name="Net" type="empty">
                <property name="ThemeName" type="string" value="Calculate"/>
                <property name="IconThemeName" type="string" value="Calculate"/>
                <property name="DoubleClickTime" type="int" value="400"/>
              </property>
              <property name="Xft" type="empty">
                <property name="Antialias" type="int" value="1"/>
                <property name="HintStyle" type="string" value="hintmedium"/>
              </property>
            </channel>
        '''

        net_dict = OrderedDict({('', 'property', 'ThemeName', 'string'): "Calculate",
                                ('', 'property', 'IconThemeName', 'string'): "Calculate",
                                ('', 'property', 'DoubleClickTime', 'int'): '400'})

        xft_dict = OrderedDict({('', 'property', 'Antialias', 'int'): "1",
                                ('', 'property', 'HintStyle', 'string'): 'hintmedium'})

        channel_content = OrderedDict({('', 'property', 'Net', 'empty'): net_dict,
                                       ('', 'property', 'Xft', 'empty'): xft_dict})

        result = OrderedDict({('', 'channel', 'xsettings', '1.0'): channel_content})

        xml_xfce_object = XMLXfceFormat(document_text, '/path/to/template')
        assert xml_xfce_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/xml_xfce/'
                  'original.xml', 'r') as original_file:
            original_text = original_file.read()
            xml_xfce_original_object = XMLXfceFormat(original_text,
                                                     '/path/to/template')

        with open('./tests/templates/format/testfiles/xml_xfce/'
                  'template.xml', 'r') as template_file:
            template_text = template_file.read()
            xml_xfce_template_object = XMLXfceFormat(template_text,
                                                     '/path/to/template')

        xml_xfce_original_object.join_template(xml_xfce_template_object)

        with open('./tests/templates/format/testfiles/xml_xfce/'
                  'result.xml', 'r') as result_file:
            result_text = result_file.read()

        assert xml_xfce_original_object.document_text == result_text

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''<?xml version="1.0" encoding="UTF-8"?>
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="Antialias" type="int" value="1"/>
    <property name="HintStyle" type="string" value="hintmedium"/>
  </property>
</channel>
'''

        template_text = '''<?xml version="1.0" encoding="UTF-8"?>
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="!HintStyle" type="string" value="hintmedium"/>
  </property>
</channel>
'''

        join_result = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/template
-->
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="Antialias" type="int" value="1"/>
  </property>
</channel>
'''

        original_object = XMLXfceFormat(original_text, '/path/to/template',
                                        add_header=True)
        template_object = XMLXfceFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''<?xml version="1.0" encoding="UTF-8"?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/ancient/template
-->
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="Antialias" type="int" value="1"/>
    <property name="HintStyle" type="string" value="hintmedium"/>
  </property>
</channel>
'''

        template_text = '''<?xml version="1.0" encoding="UTF-8"?>
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="!HintStyle" type="string" value="hintmedium"/>
  </property>
</channel>
'''

        join_result = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/template
-->
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="Antialias" type="int" value="1"/>
  </property>
</channel>
'''

        original_object = XMLXfceFormat(original_text, '/path/to/template',
                                        add_header=True)
        template_object = XMLXfceFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''<?xml version="1.0" encoding="UTF-8"?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/ancient/template
-->
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="Antialias" type="int" value="1"/>
    <property name="HintStyle" type="string" value="hintmedium"/>
  </property>
</channel>
'''

        template_text = '''<?xml version="1.0" encoding="UTF-8"?>
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="!HintStyle" type="string" value="hintmedium"/>
  </property>
</channel>
'''

        join_result = '''<?xml version='1.0' encoding='UTF-8'?>
<!--
Modified by Calculate Utilities 4.0
Processed template files:
/path/to/ancient/template
/path/to/template
-->
<channel name="xsettings" version="1.0">
  <property name="Xft" type="empty">
    <property name="Antialias" type="int" value="1"/>
  </property>
</channel>
'''

        original_object = XMLXfceFormat(original_text, '/path/to/template',
                                        add_header=True, already_changed=True)
        template_object = XMLXfceFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
