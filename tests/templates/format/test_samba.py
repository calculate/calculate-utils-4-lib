import pytest
from collections import OrderedDict
from calculate.templates.format.samba_format import SambaFormat


@pytest.mark.formats
@pytest.mark.samba
class TestParsingMethods:
    def test_if_input_document_contains_just_few_parameter_lines__the_initialised_object_contains_correct_dictionary(self):
        document_text = '''[section name]
            parameter name = /home/divanov/Home
            other parameter = yes'''

        param_line_1 = OrderedDict({('', 'parameter name'): ['/home/divanov/Home']})
        param_line_2 = OrderedDict({('', 'other parameter'): ['yes']})

        result = OrderedDict({('', 'section name'): OrderedDict(**param_line_1,
                                                                **param_line_2)})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_if_input_document_contains_few_parameter_lines_and_some_empty_lines__the_initialized_object_contains_correct_dictionary(self):
        document_text = '''
        [section name1]
            parameter name = /home/divanov/Home

            other parameter = yes


            second parameter = 1

        [section name2]

            other parameter = yes

            '''

        section_1_content = OrderedDict({('', 'parameter name'):
                                         ['/home/divanov/Home'],
                                         ('', 'other parameter'): ['yes'],
                                         ('', 'second parameter'): ['1']})

        section_2_content = OrderedDict({('', 'other parameter'): ['yes']})

        result = OrderedDict({('', 'section name1'): section_1_content,
                              ('', 'section name2'): section_2_content})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_if_input_document_contains_sections_with_similar_names_but_different_parameters__the_parameters_merged_in_one_section(self):
        document_text = '''[section name]
            parameter name = /home/divanov/Home
                            other parameter = yes

            [section name]
                            another parameter = 1'''

        section_content = OrderedDict({('', 'parameter name'):
                                       ['/home/divanov/Home'],
                                       ('', 'other parameter'): ['yes'],
                                       ('', 'another parameter'): ['1']})
        result = OrderedDict({('', 'section name'): section_content})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_if_input_document_contains_sections_with_parameters_with_action_marks__the_key_tuples_of_object_s_dictionary_have_it_as_its_first_element(self):
        document_text = '''[section name]
            -parameter name = /home/divanov/Home
                            other parameter = yes
                            !another parameter = 1'''

        section_content = OrderedDict({('-', 'parameter name'):
                                       ['/home/divanov/Home'],
                                       ('', 'other parameter'): ['yes'],
                                       ('!', 'another parameter'): ['1']})
        result = OrderedDict({('', 'section name'): section_content})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_if_parameter_in_input_document_has_some_comments__the_comments_will_be_collected_in_the_list_of_parameter_value(self):
        document_text = '''# Comment1
        [section name1]
                          parameter name = /home/divanov/Home

                          # Comment2
                          # Comment3
                          other parameter = yes

        [section name2]
                          #Comment
                          !another parameter = 1'''

        section = OrderedDict({'#': ['# Comment1'],
                              ('', 'parameter name'): ['/home/divanov/Home'],
                              ('', 'other parameter'): ['# Comment2',
                                                        '# Comment3',
                                                        'yes']})

        result = OrderedDict({('', 'section name1'): section,
                              ('', 'section name2'): OrderedDict({
                                              ('!', 'another parameter'):
                                              ['#Comment', '1']})})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_if_the_IgnoreComments_flag_is_set__the_parser_ignores_all_comments(self):
        document_text = '''# Comment1
        [section name1]
                          parameter name = /home/divanov/Home

                          # Comment2
                          # Comment3
                          other parameter = yes

        [section name2]
                          #Comment
                          !another parameter = 1'''

        section_1 = OrderedDict({('', 'parameter name'):
                                 ['/home/divanov/Home'],
                                 ('', 'other parameter'): ['yes']})

        section_2 = OrderedDict({('!', 'another parameter'): ['1']})

        result = OrderedDict({('', 'section name1'): section_1,
                              ('', 'section name2'): section_2})

        samba_object = SambaFormat(document_text, '/path/to/template',
                                   ignore_comments=True)
        assert samba_object._document_dictionary == result

    def test_if_input_document_contains_parameters_to_delete_without_assign_symbol_and_any_values_and_sections_to_delete__the_document_object_contains_dictionary_with_item_to_delete(self):
        document_text = '''
        [section name1]
            !parameter name = /home/divanov/Home
            !other parameter =
            !another parameter
        [!section name2]
            parameter = no
        [-section name3]
            parameter = no'''

        section_1 = OrderedDict({('!', 'parameter name'):
                                 ['/home/divanov/Home'],
                                ('!', 'other parameter'): [],
                                ('!', 'another parameter'): []})
        section_2 = OrderedDict({('', 'parameter'): ['no']})

        result = OrderedDict({('', 'section name1'): section_1,
                              ('!', 'section name2'): section_2,
                              ('-', 'section name3'): section_2})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_if_joinBefore_flag_is_set__the_document_object_contains_dictionary_with_sections_added_in_the_top_of_it(self):
        original_text = '''# Comment1
        [section name2]
                          parameter name = /home/divanov/Home

                          # Comment2
                          # Comment3
                          other parameter = yes

        [section name3]
                          #Comment
                          another parameter = 1'''

        template_text = '''
        [section name1]
            parameter name = /homeless/poorness

            one more parameter = oh no

        [section name3]
            # Comment
            unspoken parameter = 1'''

        result = OrderedDict({('', 'section name1'):
                              OrderedDict({('', 'parameter name'):
                                           ['/homeless/poorness'],
                                           ('', 'one more parameter'):
                                           ['oh no']}),
                              ('', 'section name2'):
                              OrderedDict({'#': ['# Comment1'],
                                           ('', 'parameter name'):
                                           ['/home/divanov/Home'],
                                           ('', 'other parameter'):
                                           ['# Comment2', '# Comment3', 'yes']}),
                              ('', 'section name3'):
                              OrderedDict({('', 'another parameter'):
                                           ['#Comment', '1'],
                                           ('', 'unspoken parameter'):
                                           ['1']})})

        samba_original_object = SambaFormat(original_text, '/path/to/template',
                                            join_before=True)
        samba_template_object = SambaFormat(template_text, '/path/to/template',
                                            ignore_comments=True)
        samba_original_object.join_template(samba_template_object)

        assert samba_original_object._document_dictionary == result

    def test_if_input_document_parameters_contains_upper_case_symbols__it_becomes_lower_case(self):
        document_text = '''[SECTION]
            parameter NAME = /home/divanov/Home
            Other Parameter = yes'''

        param_line_1 = OrderedDict({('', 'parameter name'):
                                    ['/home/divanov/Home']})
        param_line_2 = OrderedDict({('', 'other parameter'): ['yes']})

        result = OrderedDict({('', 'section'): OrderedDict(**param_line_1,
                                                           **param_line_2)})

        samba_object = SambaFormat(document_text, '/path/to/template')
        assert samba_object._document_dictionary == result

    def test_joining_documents_1(self):
        with open('./tests/templates/format/testfiles/samba/original',
                  'r') as original_file:
            original_text = original_file.read()
            samba_original_object = SambaFormat(original_text,
                                                '/path/to/template')

        with open('./tests/templates/format/testfiles/samba/template',
                  'r') as template_file:
            template_text = template_file.read()
            samba_template_object = SambaFormat(template_text,
                                                '/path/to/template',
                                                ignore_comments=True)

        samba_original_object.join_template(samba_template_object)

        with open('./tests/templates/format/testfiles/samba/result',
                  'r') as result_file:
            result_text = result_file.read()

        assert samba_original_object.document_text == result_text

    def test_make_template(self):
        document_1 = '''
        # comment
        [section name 2]
            # comment 2
            parameter name = /home/divanov/Home
            other parameter = yes
            weird parameter = something strange
        [section name 3]
            another parameter = 1'''
        document_2 = '''# comment
[section name 2]
    # comment 2
    parameter name = /home/divanov/Home
    other parameter = yes

[section name 3]
    unspoken parameter = 1
    unbelievable parameter = Mystical

[section name 1]
    parameter name = /homeless/poorness
    one more parameter = oh no
'''

        document_1_object = SambaFormat(document_1, '/path/to/template')
        document_2_object = SambaFormat(document_2, '/path/to/template')
        template_object = document_1_object.make_template(document_2_object)
        document_1_object.join_template(template_object)
        assert document_1_object.document_text == document_2

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''# Comment1
[section name2]
                  parameter name = /home/divanov/Home

                  # Comment2
                  # Comment3
                  other parameter = yes

[section name3]
                  #Comment
                  another parameter = 1'''

        template_text = '''[section name1]
    parameter name = /homeless/poorness

    one more parameter = oh no

[section name3]
    # Comment
    unspoken parameter = 1'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# Comment1
[section name2]
    parameter name = /home/divanov/Home
    # Comment2
    # Comment3
    other parameter = yes

[section name3]
    #Comment
    another parameter = 1
    # Comment
    unspoken parameter = 1

[section name1]
    parameter name = /homeless/poorness
    one more parameter = oh no
'''

        original_object = SambaFormat(original_text, '/path/to/template',
                                      add_header=True)
        template_object = SambaFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# Comment1
[section name2]
                  parameter name = /home/divanov/Home

                  # Comment2
                  # Comment3
                  other parameter = yes

[section name3]
                  #Comment
                  another parameter = 1'''

        template_text = '''[section name1]
    parameter name = /homeless/poorness

    one more parameter = oh no

[section name3]
    # Comment
    unspoken parameter = 1'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
# Comment1
[section name2]
    parameter name = /home/divanov/Home
    # Comment2
    # Comment3
    other parameter = yes

[section name3]
    #Comment
    another parameter = 1
    # Comment
    unspoken parameter = 1

[section name1]
    parameter name = /homeless/poorness
    one more parameter = oh no
'''

        original_object = SambaFormat(original_text, '/path/to/template',
                                      add_header=True)
        template_object = SambaFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
# Comment1
[section name2]
                  parameter name = /home/divanov/Home

                  # Comment2
                  # Comment3
                  other parameter = yes

[section name3]
                  #Comment
                  another parameter = 1'''

        template_text = '''[section name1]
    parameter name = /homeless/poorness

    one more parameter = oh no

[section name3]
    # Comment
    unspoken parameter = 1'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
# Comment1
[section name2]
    parameter name = /home/divanov/Home
    # Comment2
    # Comment3
    other parameter = yes

[section name3]
    #Comment
    another parameter = 1
    # Comment
    unspoken parameter = 1

[section name1]
    parameter name = /homeless/poorness
    one more parameter = oh no
'''

        original_object = SambaFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True)
        template_object = SambaFormat(template_text, '/path/to/template')
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
