import pytest
from calculate.templates.format.regex_format import RegexFormat
from calculate.templates.template_engine import ParametersContainer


@pytest.mark.formats
@pytest.mark.regex
class TestParsingMethods:
    def test_if_input_patch_document_contains_only_regular_expressions_without_any_regex_flags__it_correctly_patches_input_document(self):
        input_text = '''
First line
Another meaningless line.
TEXT&DATA
ParameterName = Value
Another line of endless sadness.
            '''

        output_text = '''
First line
Another meaningless line.
TEXT_CONFIG
ParameterName = NewValue
Another line of endless sadness.
            '''

        patch_text = '''<reg>TEXT&amp;DATA</reg>
        <text>TEXT_CONFIG</text>
        <reg>ParameterName\\s*=\\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
        <text>ParameterName = NewValue</text>
        '''

        patch_original = RegexFormat(input_text, '/path/to/template')
        patch_template = RegexFormat(patch_text, '/path/to/template')
        patch_original.join_template(patch_template)

        assert patch_original.document_text == output_text

    def test_if_input_patch_document_contains_regular_expressions_with_global_regex_flags_and_flags_as_attributes__it_correctly_patches_input_document_using_regex_flags(self):
        input_text = '''
#============================== Share Definitions =============================
[homes]
    comment = Home Directories
    browseable = no
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    guest ok = yes
    writable = no
    share modes = no
            '''

        output_text = '''
#============================== Share Definitions =============================
[homes]
    comment = Home Directories
    browseable = who knows
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    writable = yes
    share modes = no
            '''

        patch_text = r'''
        <reg dotall="1" multiline="0">(\[netlogon\].*)writable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
        <text>\1writable = yes</text>
        <reg multiline="false" dotall="True">(\[homes\].*)browseable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
        <text>\1browseable = who knows</text>
        <reg>(\[netlogon\].*)^\s*guest ok\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*\n</reg>
        <text>\1</text>
        '''

        parameters = ParametersContainer({'multiline': True, 'dotall': True})

        patch_original = RegexFormat(input_text, '/path/to/template',
                                     parameters=parameters)
        patch_template = RegexFormat(patch_text, '/path/to/template',
                                     parameters=parameters)
        patch_original.join_template(patch_template)

        assert patch_original.document_text == output_text

    def test_if_input_documents_are_an_original_document_without_calculate_header_and_a_template_and_the_add_header_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        parameters = ParametersContainer({'multiline': True, 'dotall': True})
        original_text = '''[homes]
    comment = Home Directories
    browseable = no
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    guest ok = yes
    writable = no
    share modes = no
'''

        template_text = r'''
<reg dotall="1" multiline="0">(\[netlogon\].*)writable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
<text>\1writable = yes</text>
<reg multiline="false" dotall="True">(\[homes\].*)browseable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
<text>\1browseable = who knows</text>
<reg>(\[netlogon\].*)^\s*guest ok\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*\n</reg>
<text>\1</text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
[homes]
    comment = Home Directories
    browseable = who knows
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    writable = yes
    share modes = no
'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_not_set__the_format_object_removes_calculate_header_from_original_document__joins_an_original_document_and_a_template_and_adds_the_calculate_header(self):
        parameters = ParametersContainer({'multiline': True, 'dotall': True})
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
[homes]
    comment = Home Directories
    browseable = no
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    guest ok = yes
    writable = no
    share modes = no
'''

        template_text = r'''
<reg dotall="1" multiline="0">(\[netlogon\].*)writable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
<text>\1writable = yes</text>
<reg multiline="false" dotall="True">(\[homes\].*)browseable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
<text>\1browseable = who knows</text>
<reg>(\[netlogon\].*)^\s*guest ok\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*\n</reg>
<text>\1</text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
[homes]
    comment = Home Directories
    browseable = who knows
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    writable = yes
    share modes = no
'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_if_input_documents_are_an_original_document_with_calculate_header_and_a_template_the_add_header_flag_is_set_and_the_already_changed_flag_is_set__the_format_object_joins_an_original_document_and_a_template_and_adds_the_calculate_header_with_a_template_paths_from_the_old_header_and_paths_to_a_current_template(self):
        parameters = ParametersContainer({'multiline': True, 'dotall': True})
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
[homes]
    comment = Home Directories
    browseable = no
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    guest ok = yes
    writable = no
    share modes = no
'''

        template_text = r'''
<reg dotall="1" multiline="0">(\[netlogon\].*)writable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
<text>\1writable = yes</text>
<reg multiline="false" dotall="True">(\[homes\].*)browseable\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*</reg>
<text>\1browseable = who knows</text>
<reg>(\[netlogon\].*)^\s*guest ok\s*=\s*[a-zA-Z_][a-zA-Z_0-9]*\n</reg>
<text>\1</text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
[homes]
    comment = Home Directories
    browseable = who knows
    writable = yes

# Un-comment the following and create the netlogon directory for Domain Logons
[netlogon]
    comment = Network Logon Service
    path = /var/lib/samba/netlogon
    writable = yes
    share modes = no
'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_format_regex_empty_text_tag(self):
        parameters = ParametersContainer({'multiline': True, 'dotall': True})
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
Hello
'''

        template_text = r'''
<reg>Hello</reg>
<text></text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------

'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_format_regex_does_not_strip_values_in_tags(self):
        parameters = ParametersContainer({'multiline': True, 'dotall': True})
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
Hello

World


World
'''

        template_text = r'''
<reg>


World</reg>
<text>WWWW</text>
<reg>\Z</reg>
<text>

Testing
</text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
Hello

WorldWWWW


Testing
'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_format_with_empty_comment_parameter(self):
        parameters = ParametersContainer({'multiline': True,
                                          'dotall': True,
                                          'comment': ''})
        original_text = '''
Hello
'''

        template_text = r'''
<reg>Hello</reg>
<text></text>
'''

        join_result = '''

'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_format_with_empty_comment_parameter_and_with_header_in_original_document(self):
        parameters = ParametersContainer({'multiline': True,
                                          'dotall': True,
                                          'comment': ''})
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
Hello
'''

        template_text = r'''
<reg>Hello</reg>
<text></text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------

'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_shebang(self):
        parameters = ParametersContainer({'multiline': True,
                                          'dotall': True,
                                          'comment': '#'})
        original_text = '''echo "old message"'''

        template_text = r'''
<reg>old</reg>
<text>new</text>
<reg>\Z</reg>
<text>
exit 0</text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/template
#-------------------------------------------------------------------------------
echo "new message"
exit 0'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=False,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result

    def test_shebang_with_comment(self):
        parameters = ParametersContainer({'multiline': True,
                                          'dotall': True,
                                          'comment': '#'})
        original_text = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
#-------------------------------------------------------------------------------
echo "old message"'''

        template_text = r'''
<reg>old</reg>
<text>new</text>
<reg>\Z</reg>
<text>
exit 0</text>
'''

        join_result = '''#-------------------------------------------------------------------------------
# Modified by Calculate Utilities 4.0
# Processed template files:
# /path/to/ancient/template
# /path/to/template
#-------------------------------------------------------------------------------
echo "new message"
exit 0'''

        original_object = RegexFormat(original_text, '/path/to/template',
                                      add_header=True, already_changed=True,
                                      parameters=parameters)
        template_object = RegexFormat(template_text, '/path/to/template',
                                      parameters=parameters)
        original_object.join_template(template_object)
        assert original_object.document_text == join_result
