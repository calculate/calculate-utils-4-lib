import pytest
from calculate.templates.format.base_format import Format


@pytest.mark.formats
@pytest.mark.base
class TestJoinMethod:
    def test_if_inputs_are_dictionaries_with_string_keys_without_any_action_marks__the_dictionaties_just_merged(self, StringDictionaries):
        BaseObject = Format([])
        Original, Template, Result = StringDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_inputs_are_dictionaries_with_tuple_keys_without_any_action_marks_as_their_keys__the_dictionaries_just_merged(self, TupleDictionaries):
        BaseObject = Format([])
        Original, Template, Result = TupleDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_inputs_are_dictionaries_with_same_sections_which_contain_different_parameters__a_section_from_the_template_added_to_the_same_section_of_original_dictionary(self, MergeSectionDictionaries):
        BaseObject = Format([])
        Original, Template, Result = MergeSectionDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_inputs_are_dictionaries_with_parameters_with_same_name_in_same_section__parameters_values_in_original_dictionary_changed_to_values_from_template(self, ChangeParameterDictionaries):
        BaseObject = Format([])
        Original, Template, Result = ChangeParameterDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_input_template_dictionary_has_delete_mark_for_section__section_will_be_deleted(self, DeleteSectionDictionaries):
        BaseObject = Format([])
        Original, Template, Result = DeleteSectionDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_input_template_dictionary_has_replace_mark_for_section__section_will_be_deleted(self, ReplaceSectionDictionaries):
        BaseObject = Format([])
        Original, Template, Result = ReplaceSectionDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_input_template_dictionary_has_delete_mark_for_parameter__parameter_will_be_deleted(self, DeleteParameterDictionaries):
        BaseObject = Format([])
        Original, Template, Result = DeleteParameterDictionaries
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result

    def test_if_input_dictionaries_have_no_sections_and_have_only_parameter_lines__it_will_be_processed_correctly(self, DictionariesWithoutSections):
        BaseObject = Format([])
        Original, Template, Result = DictionariesWithoutSections
        BaseObject._join(Original, Template, join_before=False)
        assert Original == Result


@pytest.mark.base
class TestLogicLinesMethod:
    def test_if_input_is_text_document_the_method_returns_list_of_its_lines(self):
        with open('./tests/templates/format/testfiles/base/'
                  'logic_lines_test.txt', 'r') as InputFile:
            InputText = InputFile.read()

        processingMethods = []
        OutputLines = ['First string of test file.',
                       'Second string of test file.',
                       'Third string of test file.',
                       'Fourth string of test file.']

        BaseObject = Format(processingMethods)
        InputLines = BaseObject._get_list_of_logic_lines(InputText)
        assert InputLines == OutputLines

    def test_if_lines_in_document_divided_using_backslash_as_continuation_symbol__method_returns_list_of_full_lines(self):
        with open('./tests/templates/format/testfiles/base/'
                  'logic_lines_test_input.txt', 'r') as InputFile:
            InputText = InputFile.read()

        with open('./tests/templates/format/testfiles/base/'
                  'logic_lines_test_output.txt', 'r') as OutputFile:
            OutputText = OutputFile.read()

        BaseObject = Format([])
        InputLines = BaseObject._get_list_of_logic_lines(InputText)
        OutputLines = BaseObject._get_list_of_logic_lines(OutputText)
        assert InputLines == OutputLines
