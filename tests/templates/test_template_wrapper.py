import pytest
import os
from calculate.templates.template_engine import ParametersContainer
from calculate.templates.template_processor import TemplateWrapper, FILE, DIR,\
                                                   TemplateTypeConflict,\
                                                   TemplateCollisionError,\
                                                   TemplateExecutorError
from calculate.utils.package import PackageAtomName, Version
from calculate.utils.files import join_paths
import hashlib


CHROOT_PATH = os.path.join(os.getcwd(),
                           'tests/templates/testfiles/test_wrapper_root')
CONFIG_ARCHIVE_PATH = os.path.join(CHROOT_PATH,
                                   'var/lib/calculate/config-archive')

test_package_name = PackageAtomName(
                        {'pkg_path': os.path.join(
                                  CHROOT_PATH,
                                  'var/db/pkg/test-category/test-package-1.0'),
                         'version': Version('1.0')})


@pytest.mark.template_wrapper
def test_if_the_TemplateWrapper_object_has_already_been_created__it_contains_the_correct_list_of_protected_and_unprotected_paths():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    TemplateWrapper._protected_is_set = False

    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)

    assert template_wrapper._protected_is_set
    assert (join_paths(CHROOT_PATH, '/etc') in
            template_wrapper._protected_set)

    mask_paths = os.environ.get('CONFIG_PROTECT_MASK', False)

    if mask_paths:
        mask_set = set()
        for path in mask_paths.split():
            mask_set.add(join_paths(CHROOT_PATH, path))
        assert template_wrapper._unprotected_set == mask_set


# Тестируем проверку типов шаблонов и файлов и поиск конфликтов.
# Тесты типов.


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_and_target_file_does_not_exist__the_TemplateWrapper_target_type_is_None():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/none'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_type is None
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/none')


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_DIR__the_TemplateWrapper_target_type_is_DIR():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join'})
    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_type == DIR
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/dir_2')


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_and_target_file_is_FILE__the_TemplateWrapper_target_type_is_FILE():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_type == FILE
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/file.conf')

# Тесты проверки конфликтов.
# Если шаблон -- файл.


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_and_target_file_is_FILE_and_force_parameter_is_set__the_TemplateWrapper_target_type_is_FILE_and_target_path_is_same():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'force': True})
    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_type == FILE
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/file.conf')


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_but_target_file_is_DIR_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_but_target_file_is_a_link_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file_link'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_but_target_file_is_DIR_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_but_target_file_is_a_link_to_a_DIR_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_link'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_but_target_file_is_a_link_to_a_FILE_and_force_parameter_is_set__the_TemplateWrapper_changes_its_target_path_to_the_link_source_file():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'force': True})

    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file_link'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_path_is_changed
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/file')

# Если шаблон -- директория.


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_DIR_and_force_parameter_is_set__the_TemplateWrapper_target_type_is_DIR_and_target_path_is_the_same():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'force': True})
    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_type == DIR
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/dir_2')


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_a_link_to_a_DIR_and_force_parameter_is_not_set__the_TemplateWrapper_changes_its_target_path_to_the_link_source_file():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join'})

    template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_link'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    assert template_wrapper.target_path_is_changed
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/dir_2')


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_but_target_file_is_FILE_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_a_link_to_a_file_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file_link'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_FILE_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_the_link_to_a_FILE_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file_link'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_and_target_file_is_the_link_to_a_DIR_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_link'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


# Если шаблон создает ссылку.


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_with_the_append_link_parameter_and_target_file_is_the_link_to_a_FILE__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file_link'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_with_the_append_link_parameter_and_target_file_is_the_link_to_a_DIR__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_link'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_with_the_append_link_parameter_and_target_file_is_the_link_to_a_FILE__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file_link'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_with_the_append_link_parameter_and_target_file_is_the_link_to_a_DIR__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_link'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_with_the_append_link_parameter_and_target_file_is_FILE_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_with_the_append_link_parameter_and_target_file_is_DIR_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_with_the_append_link_parameter_and_target_file_is_FILE_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_with_the_append_link_parameter_and_target_file_is_DIR_and_force_parameter_is_not_set__the_TemplateWrapper_throws_TemplateTypeConflict_exception():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link'})
    with pytest.raises(TemplateTypeConflict):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_with_the_append_link_parameter_and_target_file_is_FILE_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_FILE_with_the_append_link_parameter_and_target_file_is_DIR_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_with_the_append_link_parameter_and_target_file_is_FILE_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_template_type_is_DIR_with_the_append_link_parameter_and_target_file_is_DIR_and_force_parameter_is_set__the_TemplateWrapper_sets_remove_original_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'link',
                                             'force': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/dir_2'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original

# Тестируем определитель пакетов и проверку на коллизии.

@pytest.mark.template_wrapper
def test_if_template_contains_append_parameters_but_does_not_have_package_parameter_and_target_file_does_not_belong_to_any_package__the_TemplateWrapper_throws_TemplateCollisionError_exception():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    with pytest.raises(TemplateCollisionError):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_template_contains_package_parameter_and_target_file_does_not_belong_to_any_package__the_TemplateWrapper_uses_package_from_parameter_and_sets_target_without_package_flag():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.target_without_package


@pytest.mark.template_wrapper
def test_if_template_does_not_have_package_parameter_but_target_file_belongs_to_the_package__the_TemplateWrapper_uses_package_from_parameter():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert not template_wrapper.target_without_package
    assert template_wrapper.target_package_name == test_package_name


@pytest.mark.template_wrapper
def test_if_template_contains_package_parameter_and_target_file_belongs_to_the_package_from_package_parameter__the_TemplateWrapper_uses_package_from_them():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.target_package_name == test_package_name


@pytest.mark.template_wrapper
def test_if_template_contains_package_parameter_but_target_file_belongs_to_the_other_package__the_TemplateWrapper_throws_TemplateCollisionError():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    with pytest.raises(TemplateCollisionError):
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/other_file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_TemplateWrapper_object_contains_package_value__the_wrapper_can_use_Package_object_for_changing_CONTENTS_file_of_the_package():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert '/etc/dir/file.conf' in template_wrapper.target_package


# Тестируем проверку наличия пользовательских изменений.
# Сначала вспомогательные функции.


@pytest.mark.template_wrapper
def test_if_a_target_file_directory_contains_no_cfg_files_but_it_is_necessary_to_create_one__the_first_cfg_file_should_be_named_cfg0001_file():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'bind'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_0/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    cfg_name = template_wrapper._get_cfg_path(template_wrapper.target_path)
    assert cfg_name == join_paths(CHROOT_PATH, '/etc/dir_0/._cfg0000_file')


@pytest.mark.template_wrapper
def test_if_a_target_file_directory_contains_some_cfg_files_including_the_first_one__the_next_cfg_file_should_be_named_with_the_number_following_the_number_of_the_last_one():
    parameters_object = ParametersContainer({'append': 'bind'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_1/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    cfg_name = template_wrapper._get_cfg_path(join_paths(
                                                        CHROOT_PATH,
                                                        '/etc/dir_1/file'))
    assert cfg_name == join_paths(CHROOT_PATH, '/etc/dir_1/._cfg0003_file')


@pytest.mark.template_wrapper
def test_if_a_target_file_directory_contains_some_cfg_files_but_some_of_them_are_missed__the_next_cfg_file_should_be_named_with_the_number_following_the_number_of_the_last_one():
    parameters_object = ParametersContainer({'append': 'bind'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_2/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    cfg_name = template_wrapper._get_cfg_path(join_paths(
                                                        CHROOT_PATH,
                                                        '/etc/dir_2/file'))
    assert cfg_name == join_paths(CHROOT_PATH, '/etc/dir_2/._cfg0003_file')


@pytest.mark.template_wrapper
def test_if_get_archive_path_method_has_the_file_path_as_its_argument__it_returns_the_path_to_the_archive_version_of_the_input_file():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.archive_path == join_paths(
                                                      CONFIG_ARCHIVE_PATH,
                                                      '/etc/dir/file.conf')

# Тестируем результат установки флага protected.


@pytest.mark.template_wrapper
def test_if_a_target_file_is_located_in_a_protected_directory__the_TemplateWrapper_object_sets_the_protected_flag_as_True():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.protected


@pytest.mark.template_wrapper
def test_if_a_target_file_is_located_in_a_directory_that_is_not_protected__the_TemplateWrapper_object_sets_the_protected_flag_as_False():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/not_protected/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert not template_wrapper.protected


@pytest.mark.template_wrapper
def test_if_a_target_file_is_located_in_a_directory_from_the_protected_mask__the_TemplateWrapper_object_sets_the_protected_flag_as_False():
    if os.environ.get('CONFIG_PROTECT', False):
        parameters_object = ParametersContainer({'append': 'join',
                                                 'format': 'samba'})
        try:
            template_wrapper = TemplateWrapper(
                                       join_paths(CHROOT_PATH,
                                                  '/etc/terminfo/info.json'),
                                       parameters_object, FILE,
                                       '/path/to/template',
                                       chroot_path=CHROOT_PATH,
                                       config_archive_path=CONFIG_ARCHIVE_PATH)
        except Exception as error:
            pytest.fail("Unexpected exception: {}".format(str(error)))
        print('PROTECTED SET')
        print(template_wrapper._protected_set)

        print('UNPROTECTED SET')
        print(template_wrapper._unprotected_set)

        assert not template_wrapper.protected


# Тестируем проверку хэш-сумм и флаг получаемый в результате нее.


@pytest.mark.template_wrapper
def test_if_a_target_file_is_not_protected__the_TemplateWrapper_sets_the_contents_matching_flag_as_True():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/not_protected/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_template_contains_the_unbound_parameter__the_TemplateWrapper_sets_the_contents_matching_flag_as_True():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba',
                                             'unbound': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_0/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_target_file_does_not_exist_and_CONTENTS_file_of_the_file_s_package_does_not_contain_this_file__the_TemplateWrapper_sets_the_contents_matching_flag_as_True():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/none'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_target_file_was_deleted_by_a_user__the_TemplateWrapper_object_sets_the_contents_matching_flag_as_False():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/deleted.json'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert not template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_target_file_does_not_belong_to_any_package__the_TemplateWrapper_object_sets_the_contents_matching_flag_as_False():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert not template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_template_contains_the_autoupdate_parameter__the_TemplateWrapper_sets_the_contents_matching_flag_as_True():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'samba',
                                             'autoupdate': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_0/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_template_contains_the_autoupdate_parameter_and_a_target_file_does_not_belong_to_any_package__the_TemplateWrapper_sets_the_contents_matching_flag_as_True():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'autoupdate': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.target_without_package
    assert template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_target_file_s_hash_matches_the_hash_from_the_package_s_CONTENTS_file__the_TemplateWrapper_object_sets_the_contents_matching_flag_as_True():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.contents_matching


@pytest.mark.template_wrapper
def test_if_a_target_file_s_hash_does_not_match_the_hash_from_the_package_s_CONTENTS_file__the_TemplateWrapper_object_sets_the_contents_matching_flag_as_False():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_0/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert not template_wrapper.contents_matching


# Тестируем вывод путей к входному и выходному файлам исполнительного
# модуля.


@pytest.mark.template_wrapper
def test_if_after_the_target_file_check_contents_matching_flag_is_set_as_True_and_a_template_does_not_contain_source_parameter__the_output_and_the_input_paths_for_the_TemplateExecutor_is_the_same_and_it_is_target_path():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.input_path == join_paths(CHROOT_PATH,
                                                     '/etc/dir/file.conf')
    assert template_wrapper.output_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/file.conf')


@pytest.mark.template_wrapper
def test_if_after_the_target_file_check_contents_matching_flag_is_set_as_False_and_a_template_does_not_contain_source_parameter__the_output_path_is_the_cfg_file_path_and_the_input_path_is_the_calculate_archive_path():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'json'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_0/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.input_path == join_paths(CONFIG_ARCHIVE_PATH,
                                                     '/etc/dir_0/file')
    assert template_wrapper.output_path == join_paths(
                                               CHROOT_PATH,
                                               '/etc/dir_0/._cfg0000_file')


@pytest.mark.template_wrapper
def test_if_after_the_target_file_check_contents_matching_flag_is_set_as_True_and_a_template_contains_source_parameter__the_output_and_the_input_paths_for_the_TemplateExecutor_is_the_same_and_it_is_target_path():
    source = join_paths(CHROOT_PATH, '/etc/file')
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'json',
                                             'source': source})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.input_path == source
    assert template_wrapper.output_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/file.conf')


@pytest.mark.template_wrapper
def test_if_after_the_target_file_check_contents_matching_flag_is_set_as_False_and_a_template_contains_source_parameter__the_output_path_is_the_cfg_file_path_and_the_input_path_is_the_calculate_archive_path():
    source = join_paths(CHROOT_PATH, '/etc/file')
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'json',
                                             'source': source})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir_0/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
        for protected_path in template_wrapper._protected_set:
            print(protected_path)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.input_path == source
    assert template_wrapper.output_path == join_paths(
                                               CHROOT_PATH,
                                               '/etc/dir_0/._cfg0000_file')

# Тестируем поведение, если формат исполняемый.


# @pytest.mark.template_wrapper
# def test_if_a_target_path_is_FILE_and_a_template_is_executable__the_target_path_is_replaced_with_the_path_to_the_cwd_directory_and_there_is_no_package_type_conflicts_and_other_checks():
#     parameters_object = ParametersContainer({'append': 'join',
#                                              'format': 'patch'})
#     try:
#         template_wrapper = TemplateWrapper(
#                                    join_paths(CHROOT_PATH,
#                                               '/etc/dir/file.conf'),
#                                    parameters_object, FILE,
#                                    '/path/to/template',
#                                    chroot_path=CHROOT_PATH,
#                                    config_archive_path=CONFIG_ARCHIVE_PATH)
#     except Exception as error:
#         pytest.fail("Unexpected exception: {}".format(str(error)))
#     assert template_wrapper.target_path == join_paths(CHROOT_PATH,
#                                                       '/etc/dir')
#     assert template_wrapper.target_package is None


@pytest.mark.template_wrapper
def test_if_a_target_path_is_DIR_and_a_template_is_executable__the_target_path_is_the_same_and_there_is_no_package_type_conflicts_and_other_checks():
    parameters_object = ParametersContainer({'append': 'join',
                                             'format': 'patch'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir')
    assert template_wrapper.target_package is None


@pytest.mark.template_wrapper
def test_if_a_template_is_executable_and_contains_package_parameter__the_template_updates_the_CONTENTS_file_for_package_from_parameter():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'patch'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    assert template_wrapper.target_path == join_paths(CHROOT_PATH,
                                                      '/etc/dir/')
    assert template_wrapper.target_package_name == test_package_name


# Тестируем работу с CONTENTS через объект обертки.


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_object_successfully_initialized_as_FILE__the_object_can_be_used_for_an_adding_the_current_file_to_its_package_and_hash_will_be_calculated_automatically():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.add_to_contents()
    assert '/etc/file' in template_wrapper.target_package


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_object_successfully_initialized_as_FILE__the_object_can_be_used_for_an_adding_the_current_file_to_its_package_with_specified_hash():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.add_to_contents(file_md5='666')
    assert '/etc/file' in template_wrapper.target_package
    assert template_wrapper.target_package.\
        contents_dictionary['/etc/file']['md5'] == '666'


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_object_successfully_initialized__the_object_can_be_used_for_a_removing_current_file_from_its_package():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.remove_from_contents()
    assert '/etc/dir/file.conf' not in template_wrapper.target_package


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_object_successfully_initialized_as_DIR__the_object_can_be_used_for_a_removing_current_directory_and_all_its_contents_from_its_package():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.remove_from_contents()
    assert '/etc/dir/subdir' not in template_wrapper.target_package
    assert '/etc/dir/subdir/config.json' not in template_wrapper.\
        target_package
    assert '/etc/dir/file.conf' not in template_wrapper.target_package
    assert '/etc/dir' not in template_wrapper.target_package


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_object_successfully_initialized_with_DIR_type__the_object_can_be_used_for_a_removing_all_directory_files_from_its_package():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'clear'})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.clear_dir_contents()
    assert '/etc/dir/subdir' not in template_wrapper.target_package
    assert '/etc/dir/subdir/config.json' not in template_wrapper.\
        target_package
    assert '/etc/dir/file.conf' not in template_wrapper.target_package
    assert '/etc/dir' in template_wrapper.target_package


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_object_successfully_initialized__the_object_can_be_used_for_changing_CONTENTS_using_list_of_changed_files():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'patch'})
    changed_files = {join_paths(CHROOT_PATH,
                                '/etc/dir/subdir/config.json'): 'D',
                     join_paths(CHROOT_PATH,
                                '/etc/file'): 'M',
                     join_paths(CHROOT_PATH,
                                '/etc/dir/subdir/file'): 'M'}
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir'),
                                   parameters_object, DIR,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.update_contents_from_list(changed_files)
    assert '/etc/dir/subdir/config.json' not in template_wrapper.\
        target_package
    assert '/etc/file' in template_wrapper.target_package
    assert '/etc/dir/subdir/file' in template_wrapper.target_package

    new_md5 = hashlib.md5(open(join_paths(CHROOT_PATH,
                                          '/etc/dir/subdir/file'),
                               'r').read().encode()).hexdigest()
    assert template_wrapper.target_package.\
        contents_dictionary['/etc/dir/subdir/file']['md5'] == new_md5


@pytest.mark.template_wrapper
def test_if_a_TemplateWrapper_was_used_to_change_CONTENTS__it_should_be_saved_using_save_changes_method():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'autoupdate': True})
    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    template_wrapper.add_to_contents()
    template_wrapper.save_changes()

    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/dir/file.conf'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))
    template_wrapper.remove_from_contents()
    template_wrapper.save_changes()

    try:
        template_wrapper = TemplateWrapper(
                                   join_paths(CHROOT_PATH,
                                              '/etc/file'),
                                   parameters_object, FILE,
                                   '/path/to/template',
                                   chroot_path=CHROOT_PATH,
                                   config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert '/etc/file' in template_wrapper.target_package
    assert '/etc/dir/file.conf' not in template_wrapper.target_package

    template_wrapper.remove_from_contents()
    template_wrapper.save_changes()

    template_wrapper = TemplateWrapper(
                               join_paths(CHROOT_PATH,
                                          '/etc/dir/file.conf'),
                               parameters_object, FILE,
                               '/path/to/template',
                               chroot_path=CHROOT_PATH,
                               config_archive_path=CONFIG_ARCHIVE_PATH)
    template_wrapper.add_to_contents()
    template_wrapper.save_changes()

    assert '/etc/dir/file.conf' in template_wrapper.target_package
    assert '/etc/file' not in template_wrapper.target_package


@pytest.mark.template_wrapper
def test_if_after_removing_some_files_thought_TemplateWrapper_some_directories_become_empty__empty_directories_will_be_removed_saving_changes():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'autoupdate': True})
    try:
        template_wrapper = TemplateWrapper(
                                 join_paths(CHROOT_PATH,
                                            '/etc/dir/dir_1/config.json'),
                                 parameters_object, FILE,
                                 '/path/to/template',
                                 chroot_path=CHROOT_PATH,
                                 config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    template_wrapper.remove_from_contents()
    template_wrapper.save_changes()

    assert '/etc/dir/dir_1/config.json' not in template_wrapper.\
        target_package
    assert '/etc/dir/dir_1' not in template_wrapper.\
        target_package

    template_wrapper.add_to_contents()
    template_wrapper.save_changes()

    assert '/etc/dir/dir_1/config.json' in template_wrapper.\
        target_package
    assert '/etc/dir/dir_1' in template_wrapper.\
        target_package


# Тестируем особенности поведения при различных значениях параметров.


# @pytest.mark.template_wrapper
# def test_if_mirror_parameter_is_set_and_target_file_does_not_exist__a_TemplateWrapper_object_throws_TemplateExecutorError():
#     source = join_paths(CHROOT_PATH, '/etc/file')
#     parameters_object = ParametersContainer({'package': test_package_name,
#                                              'append': 'join',
#                                              'format': 'bind',
#                                              'source': source,
#                                              'mirror': True})
# 
#     with pytest.raises(TemplateExecutorError):
#         TemplateWrapper(join_paths(CHROOT_PATH, '/etc/dir/none'),
#                         parameters_object, FILE,
#                         '/path/to/template',
#                         chroot_path=CHROOT_PATH,
#                         config_archive_path=CONFIG_ARCHIVE_PATH)


@pytest.mark.template_wrapper
def test_if_mirror_parameter_is_set_and_file_from_the_source_parameter_does_not_exist__a_TemplateWrapper_object_sets_remove_original_flag_as_True():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'append': 'join',
                                             'format': 'samba',
                                             'source': True,
                                             'mirror': True})
    # Значение True для параметра означает его наличие.
    # Если параметр должен содержать небулево значение, например, путь, но
    # для него стоит True -- это значит, что путь оказался некорректным,
    # но есть необходимость показать, что параметр в шаблоне присутствовал.
    try:
        template_wrapper = TemplateWrapper(
                                 join_paths(CHROOT_PATH,
                                            '/etc/dir/file.conf'),
                                 parameters_object, FILE,
                                 '/path/to/template',
                                 chroot_path=CHROOT_PATH,
                                 config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.remove_original


@pytest.mark.template_wrapper
def test_if_run_parameter_is_set_for_template__no_checks_are_carried_out_while_a_TemplateWrapper_object_initialization():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'run': '/usr/bin/python'})
    try:
        template_wrapper = TemplateWrapper(
                                 join_paths(CHROOT_PATH,
                                            '/etc/dir/file.conf'),
                                 parameters_object, FILE,
                                 '/path/to/template',
                                 chroot_path=CHROOT_PATH,
                                 config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.parameters.run == '/usr/bin/python'


@pytest.mark.template_wrapper
def test_if_exec_parameter_is_set_for_template__no_checks_are_carried_out_while_a_TemplateWrapper_object_initialization():
    parameters_object = ParametersContainer({'package': test_package_name,
                                             'exec': '/usr/bin/python'})
    try:
        template_wrapper = TemplateWrapper(
                                 join_paths(CHROOT_PATH,
                                            '/etc/dir/file.conf'),
                                 parameters_object, FILE,
                                 '/path/to/template',
                                 chroot_path=CHROOT_PATH,
                                 config_archive_path=CONFIG_ARCHIVE_PATH)
    except Exception as error:
        pytest.fail("Unexpected exception: {}".format(str(error)))

    assert template_wrapper.parameters.exec == '/usr/bin/python'
