import pytest
import calculate.templates.template_filters as filters


class TestObj:
    def __str__(self):
        return "TestObj_value"


@pytest.mark.parametrize('case',
    [
        {
            "name": "empty",
            "data": "",
            "params": (),
            "result": ""
        },
        {
            "name": "none value",
            "data": None,
            "params": (),
            "result": ""
        },
        {
            "name": "obj value",
            "data": TestObj(),
            "params": (),
            "result": "TestObj_value"
        },
        {
            "name": "default",
            "data": "123",
            "params": (),
            "result": "123"
        },
        {
            "name": "default with delimiter",
            "data": "123-456",
            "params": (),
            "result": "123"
        },
        {
            "name": "second field",
            "data": "123-456",
            "params": (1,),
            "result": "456"
        },
        {
            "name": "second field empty",
            "data": "",
            "params": (1,),
            "result": ""
        },
        {
            "name": "second field but one field",
            "data": "123",
            "params": (1,),
            "result": ""
        },
        {
            "name": "use dilimeter",
            "data": "123.456",
            "params": (1,"."),
            "result": "456"
        },
    ],
    ids=lambda x:x["name"])
@pytest.mark.template_filters
def test_filter_cut(case):
    assert filters.cut(case["data"], *case["params"]) == case["result"]
