from calculate.variables.datavars import Namespace, Variable, Dependence,\
                                         StringType, HashType, TableType,\
                                         BooleanType, ListType, IntegerType

with Namespace('linux'):
    Variable('shortname', source='', type=StringType)

    Variable('ver', source='', type=StringType)

    Variable('fullname', source='', type=StringType)

    Variable('subname', source='', type=StringType)

    Variable('arch', source='', type=StringType)

    Variable('test_1', source=['choice_1',
                               'choice_2',
                               'choice_3',
                               'choice_4'], type=ListType)

    Variable('test_2', source=False, type=BooleanType)

    Variable('test_3', source='test string', type=StringType)

    Variable('test_4', source='/dev/sda', type=StringType)

    Variable('test_5', source=8, type=IntegerType)

    Variable('test_6', source="Comment", type=StringType)

    def get_title(subname, fullname, ver):
        if subname.value:
            return '{} {} {}'.format(fullname.value, subname.value, ver.value)
        else:
            return '{} {}'.format(fullname.value, ver.value)
    Variable('title', type=StringType,
             source=Dependence('.subname', '.fullname', '.ver',
                               depend=get_title))

Variable('hashvar', source={'value1': 'test1',
                            'value2': 'test2'}, type=HashType)

Variable('hashvar_0', source={'value1': 'test1',
                              'value2': 'test2'}, type=HashType)

Variable('hashvar_1', source={'key1': 'value1',
                              'key2': 'value2'}, type=HashType)

Variable('hashvar_2', source={'id_1': 1349,
                              'id_2': 1575}, type=HashType)

Variable('calculate', type=StringType,
         source=Dependence('.hashvar_0',
                           depend=lambda hashvar: "{} {}".format(
                                                     hashvar.value['value1'],
                                                     hashvar.value['value2'])))

Variable('tablevar', type=TableType, source=[{"dev": "/dev/sdb1",
                                              "mount": "/"},
                                             {"dev": "/dev/sdb2",
                                              "mount": "/var/calculate"}])

Variable('dev_table', type=TableType, source=[{"dev": "/dev/sdb1",
                                               "mount": "/"},
                                              {"dev": "/dev/sdb2",
                                               "mount": "/var/calculate"}])
