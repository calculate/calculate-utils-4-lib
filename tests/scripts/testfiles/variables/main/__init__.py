import os
from calculate.variables.datavars import Variable, StringType, ListType


TESTFILES_PATH = os.path.join(os.getcwd(), 'tests/scripts/testfiles')

Variable('cl_chroot_path', type=StringType.readonly, source='/')

Variable('cl_root_path', type=StringType.readonly, source='/')

Variable('cl_template_path', type=StringType.readonly,
         source=os.path.join(TESTFILES_PATH, 'templates'))

Variable('cl_ignore_files', type=ListType, source=['*.swp'])
