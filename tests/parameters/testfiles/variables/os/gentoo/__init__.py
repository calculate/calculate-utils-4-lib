import os
from calculate.variables.datavars import Variable, Namespace, Dependence,\
                                         StringType, TableType
'''
gentoo:
      make_profile -> string
      profile:
          path -> string
          name -> string
      repositories[*]{name, path} -> table
      config -> undefined
'''

TESTFILES_PATH = os.path.join(os.getcwd(), 'tests/parameters/testfiles')

# Путь до файла, указывающего на активный профиль
Variable('make_profile', type=StringType, source='/etc/portage/make.profile')

# Параметры текущего профиля.
with Namespace('profile'):
    # Абсолютный путь до профиля
    Variable('path', type=StringType,
             source=os.path.join(TESTFILES_PATH,
                                 "gentoo/repos/distros/profiles/CLD/amd64"))

    def get_profile_name(path, repositories):
        profile_path = path.value
        if not profile_path:
            return ""

        for repository in repositories.value:
            repository_path = repository['path']
            repository_name = repository['name']
            remove_part = os.path.normpath(os.path.join(repository_path,
                                                        "profiles"))
            if profile_path.startswith(remove_part):
                return "{}:{}".format(repository_name,
                                      profile_path[len(remove_part) + 1:])
        return profile_path
    # Название профиля
    Variable('name', type=StringType,
             source=Dependence('.path', '..repositories',
                               depend=get_profile_name))

# Информация о репозиториях
# name: имя репозитория
# path: полный путь до репозитория
Variable('repositories', type=TableType,
         source=[{'name': 'distros',
                  'path': os.path.join(TESTFILES_PATH,
                                       "gentoo/repos/distros")},
                 {'name': 'calculate',
                  'path': os.path.join(TESTFILES_PATH,
                                       "gentoo/repos/calculate")},
                 {'name': 'gentoo',
                  'path': os.path.join(TESTFILES_PATH,
                                       "gentoo/portage")}])
