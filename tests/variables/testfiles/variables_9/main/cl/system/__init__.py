import os
from calculate.variables.datavars import Variable, ListType, HashType
'''
system:
    env_order -> list
    env_path -> hash
'''


TESTFILES_PATH = os.path.join(os.getcwd(), 'tests/variables/testfiles')


# Список мест, где есть calculate.ini файлы.
Variable('env_order', type=ListType, source=['system', 'local'])

# Отображение множества мест, где есть calculate.ini файлы, на пути к ним.
Variable('env_path', type=HashType,
         source={'local': os.path.join(TESTFILES_PATH,
                                       'ini_vars/calculate_4.ini'),
                 'system': os.path.join(TESTFILES_PATH,
                                        'ini_vars/calculate_5.ini')})
