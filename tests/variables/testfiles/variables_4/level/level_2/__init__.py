from calculate.variables.datavars import Variable, StringType, Dependence


Variable('vargetter', type=StringType,
         source=Dependence('main.chroot',
                           depend=lambda chroot:
                           '{} test'.format(chroot.value)))
