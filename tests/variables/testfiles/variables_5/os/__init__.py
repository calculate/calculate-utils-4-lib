from calculate.variables.datavars import Namespace, Variable, Dependence,\
                                        StringType, HashType, TableType

with Namespace('linux'):
    Variable('shortname', source='', type=StringType)

    Variable('ver', source='', type=StringType)

    Variable('fullname', source='', type=StringType)

    Variable('subname', source='', type=StringType)

    Variable('arch', source='', type=StringType)

    Variable('test', source='', type=StringType)

    Variable('test_var', type=StringType,
             source=Dependence('.test',
                               depend=lambda test: 'test = {}'.
                                                   format(test.value)))

    def get_title(subname, fullname, ver):
        if subname.value:
            return '{} {} {}'.format(fullname.value, subname.value, ver.value)
        else:
            return '{} {}'.format(fullname.value, ver.value)
    Variable('title', type=StringType,
             source=Dependence('.subname', '.fullname', '.ver',
                               depend=get_title))

Variable('hashvar', source={'value1': 'test1',
                            'value2': 'test2'}, type=HashType)

Variable('calculate', type=StringType,
         source=Dependence('.hashvar',
                           depend=lambda hashvar: "{} {}".format(
                                                     hashvar.value['value1'],
                                                     hashvar.value['value2'])))

Variable('tablevar', type=TableType, source=[{"dev": "/dev/sdb1",
                                              "mount": "/"},
                                             {"dev": "/dev/sdb2",
                                              "mount": "/var/calculate"}])
