from calculate.variables.datavars import Variable, StringType, Calculate


class A:
    @classmethod
    def test(cls):
        return "test_0"


class B:
    def __init__(self):
        self._value = "test_1"

    def test(self):
        return self._value


b = B()


Variable("test_0", type=StringType, source=Calculate(A.test))
Variable("test_1", type=StringType, source=Calculate(b.test))

Variable('chroot', type=StringType.readonly, source='/')
