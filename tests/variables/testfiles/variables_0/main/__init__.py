from calculate.variables.datavars import Namespace, Variable, StringType,\
                                         IntegerType, BooleanType

Variable('old_variable', source='ancient_value', type=StringType)

Variable('bool_variable', source=True, type=BooleanType)
