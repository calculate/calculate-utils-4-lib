from calculate.variables.datavars import Namespace, Variable, StringType,\
                                         IntegerType, BooleanType

Variable('strange_variable', source='weird_value', type=StringType)

Variable('plain_variable', source=True, type=BooleanType)
