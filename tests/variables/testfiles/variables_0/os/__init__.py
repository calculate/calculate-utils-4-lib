from calculate.variables.datavars import Namespace, Variable, StringType,\
                                         IntegerType

with Namespace('ns'):
    Variable('var_1', source='value_1', type=StringType)
    Variable('var_2', source=2, type=IntegerType)
