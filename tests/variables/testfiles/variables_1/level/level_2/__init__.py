def import_variables():
    from calculate.variables.datavars import Variable, StringType, Calculate

    Variable('vargetter', type=StringType,
             source=Calculate(lambda chroot: '{} test'.format(chroot.value),
                              'main.chroot'))
