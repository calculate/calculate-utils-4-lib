def import_variables():
    from calculate.variables.datavars import (
        Namespace,
        Variable,
        Dependence,
        StringType,
        HashType,
        TableType,
        ListType,
        FloatType
        )

    Variable('simple', type=StringType, source='simple value')

    Variable('use_local_simple', type=StringType,
             source=Dependence('.simple',
                               depend=lambda simple: 'Using {}'.format(
                                                                simple.value)))

    Variable('use_full_simple', type=StringType,
             source=Dependence('level.simple',
                               depend=lambda simple: 'Using {}'.format(
                                                                simple.value)))

    Variable('disks', type=ListType,
             source=["/dev/sda1", "/dev/sda2", "/dev/sda3"])

    Variable('version', type=FloatType, source='1.0')

    Variable('my_shortname', type=StringType, source='CLD')

    Variable('linux', type=HashType,
             source=Dependence('.version', '.my_shortname',
                               depend=lambda version, my_shortname:
                               {'version': version.value,
                                'shortname': my_shortname.value}))

    Variable('shortname_test', type=StringType,
             source=Dependence('.linux.shortname',
                               depend=lambda shortname: '{} test'.format(
                                                             shortname.value)))

    Variable('device_list', type=ListType,
             source=["/dev/sda", "/dev/sdb"])

    def get_device_table(device_list):
        map_data = {'/dev/sda': ["hdd", "Samsung SSD"],
                    '/dev/sdb': ["flash", "Transcend 64GB"],
                    '/dev/sdc': ["usbhdd", "WD 1TB"]}
        default_value = ["hdd", "Unknown"]
        print('device_list = {}'.format(device_list.value))
        return [{"dev": device,
                 "type": map_data.get(device, default_value)[0],
                 "name": map_data.get(device, default_value)[1]}
                for device in device_list.value]

    Variable('device', type=TableType, source=Dependence(
                                                  '.device_list',
                                                  depend=get_device_table))

    Variable('device_child', type=StringType,
             source=Dependence('.device',
                               depend=lambda device: device.value[0]['type']))

    with Namespace('level_3'):
        Variable('my_var_1', type=StringType, source='testing')

        Variable('my_var_2', type=StringType, source='testing_2')
