def import_variables():
    from calculate.variables.datavars import Variable, StringType

    Variable('chroot', type=StringType.readonly, source='/')
