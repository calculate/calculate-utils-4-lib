from calculate.parameters.parameters import BaseParameter, Integer,\
                                            String, ValidationError


class MyShinyParameter(BaseParameter):
    type = Integer()

    def validate(self, var):
        if var.value < 10:
            raise ValidationError("The value must be greater than 10")

    def bind_method(self, var):
        return var.value, None


class AnotherParameter(BaseParameter):
    type = String()

    def bind_method(self):
        return 'default string', None

    def validate(self, value):
        if not value.startswith('/var/lib'):
            raise ValidationError("The value must starts with a"
                                  " '/var/lib'")


class OneMoreParameter(BaseParameter):
    type = String()

    def bind_method(self):
        return 'default string', None

    def validate(self, value):
        available_set = {'mystery', 'horror', 'weird'}
        if value not in available_set:
            raise ValidationError(f"The value '{value}' is not in"
                                  f" available. Available values:"
                                  f" {', '.join(available_set)}")
