from calculate.scripts.scripts import Script, Task


def action(output, arg1: str) -> str:
    '''Задача выполняемая скриптом ниже.'''
    return f'os.calculate = {arg1}'


# Тестовый скрипт.
test_script = Script('test_script'
                     ).tasks(Task(id='task',
                                  name="Task",
                                  action=action,
                                  args=["os.calculate"]))
