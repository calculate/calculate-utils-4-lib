import os
from calculate.variables.datavars import Variable, ListType, HashType
'''
system:
    env_order -> list
    env_path -> hash
'''


TESTFILES_PATH = os.path.join(os.getcwd(), 'tests/commands/testfiles')


# Список мест, где есть calculate.ini файлы.
Variable('env_order', type=ListType, source=['system'])

# Отображение множества мест, где есть calculate.ini файлы, на пути к ним.
Variable('env_path', type=HashType,
         source={'system': os.path.join(TESTFILES_PATH, 'calculate.ini')})
