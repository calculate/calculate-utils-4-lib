import os
import shutil
import pytest

from calculate.commands.commands import Command
from calculate.parameters.parameters import Description
from calculate.variables.loader import Datavars
from calculate.utils.io_module import IOModule

from .parameters import MyShinyParameter, OneMoreParameter, AnotherParameter
from .scripts import test_script


TESTFILES_PATH = os.path.join(os.getcwd(), 'tests/commands/testfiles')


@pytest.mark.commands
class TestCommands:
    def test_to_make_testfiles(self):
        shutil.copytree(os.path.join(TESTFILES_PATH, 'gentoo.backup'),
                        os.path.join(TESTFILES_PATH, 'gentoo'),
                        symlinks=True)

    def test_commands_first(self):
        datavars = Datavars(variables_path=os.path.join(TESTFILES_PATH,
                                                        'variables'))

        command = Command(command_id='test',
                          category='Test Category',
                          title='Test',
                          script=test_script,
                          namespace='os',
                          rights=['group'],
                          parameters=[
                              MyShinyParameter
                              (
                                'my-shiny', 'The Shiniest ones',
                                Description(
                                  short='The shiny parameter',
                                  full='The shiniest thing known to science.',
                                  usage='-S COUNT, --my-shiny COUNT'),
                                shortname='S'
                              ).bind('os.linux.test_5'),
                              AnotherParameter
                              (
                                'my-other', 'The Others',
                                Description(
                                  short='Not from this world.'),
                                shortname='o'
                              ),
                              OneMoreParameter
                              (
                                'one-more', 'The Others',
                                Description(full='Plan 9 from outer space.')
                              )])

        command_runner = command.make_runner(datavars, IOModule())
        command_runner.run_command()

        assert datavars.scripts.test_script.task.result ==\
            'os.calculate = test1 test2'

    def test_commands_with_setvar(self):
        datavars = Datavars(variables_path=os.path.join(TESTFILES_PATH,
                                                        'variables'))

        test_parameters = [
            MyShinyParameter('my-shiny', 'The Shiniest ones',
                             Description(
                                   short='The shiny parameter',
                                   full='The shiniest thing known to science.',
                                   usage='-S COUNT, --my-shiny COUNT'),
                             shortname='S').bind('os.linux.test_5'),
            AnotherParameter('my-other', 'The Others',
                             Description(short='Not from this world.'),
                             shortname='o'),
            OneMoreParameter('one-more', 'The Others',
                             Description(full='Plan 9 from outer space.'))]

        command = Command(command_id='test',
                          category='Test Category',
                          title='Test',
                          script=test_script,
                          parameters=test_parameters,
                          namespace='os',
                          gui=True,
                          icon=['icon_1', 'icon_2'],
                          setvars={'os.hashvar_0':
                                   {'value1': 'new_1',
                                    'value2': 'new_2'},
                                   '.linux.test_5': 1349},
                          rights=['install'])

        command_runner = command.make_runner(datavars, IOModule())
        command_runner.run_command()

        assert datavars.os.hashvar_0.get_hash() == {'value1': 'new_1',
                                                    'value2': 'new_2'}
        assert datavars.os.linux.test_5 == 1349
        assert datavars.os.calculate == 'new_1 new_2'
        assert datavars.scripts.test_script.task.result ==\
            'os.calculate = new_1 new_2'

    def test_for_removing_testfiles(self):
        shutil.rmtree(os.path.join(TESTFILES_PATH, 'gentoo'))
