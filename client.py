import os
import asyncio
import aiohttp


async def get_root(client):
    async with client.get('http://localhost/') as resp:
        assert resp.status == 200
        return await resp.json()


async def main():
    unix_conn = aiohttp.UnixConnector(path='./input.sock')

    try:
        async with aiohttp.ClientSession(connector=unix_conn) as client:
            print('---------------------------')
            print('Request GET "/" HTTP/1.1:')
            response = await get_root(client)
            print(f'Response: {response}')
            print('---------------------------')
    except KeyboardInterrupt:
        raise
    finally:
        print('\nClosing connection...')
        await unix_conn.close()


if __name__ == '__main__':
    print(f"TESTING = {os.environ.get('TESTING', None)}")
    loop = asyncio.new_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        loop.close()
