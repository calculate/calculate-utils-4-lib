install:
	sudo python ./setup.py install --single-version-externally-managed --root=/

test: install
	pytest
